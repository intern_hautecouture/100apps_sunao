﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;

public class Share : MonoBehaviour {
    public ButtonAnimation btn;

    private string twitterBundleId = "com.twitter.android";
    private string facebookBundleId = "com.facebook.katana";

    //change this to the game's link
    private string playStoreLink = "https://google.com";
    private string appleStoreLink = "https://google.com";

    private string gameName = "";
    private string hashTag = ""; //for twitter

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public bool IsAppInstalled(string bundleID)
    {
#if UNITY_ANDROID
        AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
        AndroidJavaObject launchIntent = null;
        try
        {
            launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleID);
        }
        catch (Exception ex)
        {
            Debug.Log("exception" + ex.Message);
        }
        if (launchIntent == null)
            return false;
        return true;
#else
        //this part should be changed once the ios dll is done
         return false;
#endif
    }

    public void ShareTwitter()
    {
        string url = playStoreLink;

#if UNITY_ANDROID
        url = playStoreLink;
#else
        url = appleStoreLink;
#endif

        //change this for this game
        string str1 = gameName + "でステージ";
        string clearedLevel = "";
        string str2 = "をクリア！\n";
        string str3 = hashTag;

        //clearedLevel = GameManager.levelLoaded.ToString();

		Application.OpenURL("http://twitter.com/intent/tweet?text=" +
			WWW.EscapeURL(str1 + clearedLevel + str2 + url + str3));
        
    }

    public void ShareLine()
    {
        string storeURL = playStoreLink;

#if UNITY_ANDROID
        storeURL = playStoreLink;
#else
        storeURL = appleStoreLink;
#endif

        string str1 = gameName + "でステージ";
        string clearedLevel = "";

        string str2 = "をクリア！\n";

        //clearedLevel = GameManager.levelLoaded.ToString();

        string msg = str1 + clearedLevel + str2 + storeURL;
        string url = "http://line.me/R/msg/text/?" + System.Uri.EscapeUriString(msg);
        Application.OpenURL(url);
    }

    public void ShareFacebook()
    {
        /*if (IsAppInstalled(facebookBundleId))
        {
            UM_ShareUtility.FacebookShare("This is my text to share");
        }
        else
        {*/
            if (!FB.IsInitialized)
            {
                Debug.Log("FB Not Initialized");
                FB.Init(ShareLink);
            }
            else
            {
                //FB.ActivateApp();
                ShareLink();
            }

        /*}*/
        
    }

    void ShareLink()
    {
        System.Uri FeedLink = new System.Uri(playStoreLink);
#if UNITY_ANDROID
        FeedLink = new System.Uri(playStoreLink);
#else
        FeedLink = new System.Uri(appleStoreLink);
#endif

        string FeedLinkName = "";
        string FeedLinkDescription = "";
        string str1 = gameName;
        string str2 = gameName + "でステージ";
        string clearedLevel = "";

        string str3 = "をクリア！";

        //TODO: change image when it is finished
        string pictureURL = "https://100apps.s3.amazonaws.com/000_Original/ShareImage/kumatantatch_head.jpg";


        FeedLinkName = str1;
        FeedLinkDescription = clearedLevel + str2 + str3;

        FB.ShareLink(FeedLink, FeedLinkName, FeedLinkDescription, new System.Uri(pictureURL), callback: HandleShareResult);
    }

    public void HandleShareResult(IResult result)
    {
        Debug.Log("RESULT: " + result.ToString());
    }

}
