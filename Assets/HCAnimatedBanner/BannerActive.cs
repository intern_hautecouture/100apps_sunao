﻿using UnityEngine;
using System.Collections;

public class BannerActive : MonoBehaviour {

    public GameObject banner;

    private static BannerActive _instance = null;

    public static BannerActive Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {
        if (null == _instance)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

        // Use this for initialization
        void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void bannertrue()
    {
        banner.SetActive(true);
    }

    public void bannerfalse()
    {
        banner.SetActive(false);
    }

}
