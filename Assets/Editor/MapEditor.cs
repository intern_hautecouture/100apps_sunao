using UnityEngine;
using UnityEditor;

public class MapEditor : EditorWindow
{
    static MapEditor editor;
    static Rect window;
    static string warningMessage = "";
    string level;
    string locked = "";
    string star = "";
    string highScore = "";
    string targetScore = "";
    string time = "";
    bool isUpdate = false;
    Vector2 scrollPosition = Vector2.zero;
    static LevelData levelData;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/Map Editor")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        editor = (MapEditor)EditorWindow.GetWindow(typeof(MapEditor), true, "Map Editor");

        levelData = AssetDatabase.LoadAssetAtPath<LevelData>("Assets/Resources/LevelsData.asset");

        if (!PlayerPrefs.HasKey(MapManager.LEVEL_ + 1))
            MapManager.defaultMap(levelData);
    }

    void RestartValue()
    {
        warningMessage = "";
        level = "";
        locked = "";
        star = "";
        highScore = "";
        targetScore = "";
        time = "";
        isUpdate = false;
    }

    void OnGUI()
    {
        if (!editor)
            editor = (MapEditor)EditorWindow.GetWindow(typeof(MapEditor));

        window = editor.position;

        if (warningMessage.Length > 0)
        {
            GUI.color = Color.red;
            GUI.Box(new Rect(window.width - 320, 5, 310, 30), "");

            GUI.color = Color.yellow;
            GUI.Label(new Rect(window.width - 300, 10, 300, 20), warningMessage);
            GUI.color = Color.white;
        }
        if (!isUpdate)
            GUI.Label(new Rect(20, 10, 300, 20), "Identity (auto increment): " + (PlayerPrefs.GetInt(MapManager.NUMBER_OF_LEVEL) + 1));
        else
            GUI.Label(new Rect(20, 10, 300, 20), "Update level " + level);

        GUI.Box(new Rect(10, 40, window.width - 20, 37), "");
        locked = EditorGUI.TextField(new Rect(20, 50, 300, 17), "Locked Value:", locked);
        GUI.Label(new Rect(350, 50, window.width - 320, 20), "(1 : Unlock and 0 : Lock)");

        GUI.Box(new Rect(10, 80, window.width - 20, 37), "");
        star = EditorGUI.TextField(new Rect(20, 90, 300, 17), "Star Value:", star);
        GUI.Label(new Rect(350, 90, window.width - 320, 20), "(0, 1, 2 or 3) <-> number star of current level. Default 0");

        GUI.Box(new Rect(10, 120, window.width - 20, 37), "");
        highScore = EditorGUI.TextField(new Rect(20, 130, 300, 17), "High Score Value:", highScore);
        GUI.Label(new Rect(350, 130, window.width - 320, 20), "Input number (Default 0)");

        GUI.Box(new Rect(10, 160, window.width - 20, 37), "");
        targetScore = EditorGUI.TextField(new Rect(20, 170, 300, 17), "Target Score Value:", targetScore);
        GUI.Label(new Rect(350, 170, window.width - 320, 20), "Input number");

        GUI.Box(new Rect(10, 200, window.width - 20, 37), "");
        time = EditorGUI.TextField(new Rect(20, 210, 300, 17), "Time Value:", time);
        GUI.Label(new Rect(350, 210, window.width - 320, 20), "Input number (second)");

        //GUI.Box(new Rect(10, 200, window.width - 20, 37), "");
        //EditorGUI.ObjectField(new Rect(20, 210, 300, 17), );

        if (!isUpdate)
        {
            if(GUI.Button(new Rect(window.width-200,290,90,17),"Save"))
            {
                levelData.Reset();
                levelData.levelNumber = PlayerPrefs.GetInt(MapManager.NUMBER_OF_LEVEL);
                for (int i = 1; i <= PlayerPrefs.GetInt(MapManager.NUMBER_OF_LEVEL); i++)
                {
                    levelData.levelTargetScore.Add(PlayerPrefs.GetInt(MapManager.TARGET_ + i));
                    levelData.levelTimes.Add(Mathf.RoundToInt(PlayerPrefs.GetFloat(MapManager.GAME_TIME_ + i)));
                }

                AssetDatabase.Refresh();
                EditorUtility.SetDirty(levelData);
                AssetDatabase.SaveAssets();

                //Debug.Log("levelNumber: " + levelData.levelNumber);
                //Debug.Log("leveltargetscore: " + levelData.levelTargetScore.Count);
                //Debug.Log("leveltargettime: " + levelData.levelTimes.Count);
            }

            if (GUI.Button(new Rect(window.width - 110, 290, 90, 17), "Add"))
            {
                if (string.IsNullOrEmpty(locked) || string.IsNullOrEmpty(star) || string.IsNullOrEmpty(targetScore) || string.IsNullOrEmpty(highScore) || string.IsNullOrEmpty(time))
                {
                    warningMessage = "ERROR: MISSING VALUE !";
                    return;
                }
                else
                {
                    int parsedStar = 0;
                    int parsedHighScore = 0;
                    int parsedTargetScore = 0;
                    float parsedTime = 0;
                    string tmpLocked = "";

                    if (locked.Equals("0") || locked.Equals("1"))
                    {
                        tmpLocked = locked;
                    }
                    else
                    {
                        warningMessage = "ERROR: LOCKED INVALID VALUE !";
                        return;
                    }

                    int tmpStar;

                    if (int.TryParse(star, out parsedStar))
                    {
                        tmpStar = parsedStar;
                    }
                    else
                    {
                        warningMessage = "ERROR: STAR INVALID VALUE !";
                        return;
                    }

                    int tmpHighScore;

                    if (int.TryParse(highScore, out parsedHighScore))
                    {
                        tmpHighScore = parsedHighScore;
                    }
                    else
                    {
                        warningMessage = "ERROR: HIGH SCORE INVALID VALUE !";
                        return;
                    }

                    int tmpTargetScore;

                    if (int.TryParse(targetScore, out parsedTargetScore))
                    {
                        tmpTargetScore = parsedTargetScore;
                    }
                    else
                    {
                        warningMessage = "ERROR: TARGET SCORE INVALID VALUE !";
                        return;
                    }

                    float tmpTime;

                    if (float.TryParse(time, out parsedTime))
                    {
                        tmpTime = parsedTime;
                    }
                    else
                    {
                        warningMessage = "ERROR: TIME INVALID VALUE !";
                        return;
                    }


                    MapManager.AddMap(PlayerPrefs.GetInt(MapManager.NUMBER_OF_LEVEL) + 1, tmpLocked, tmpStar, tmpHighScore, tmpTargetScore, tmpTime);
                    warningMessage = "";

                }
            }
        }
        else
        {
            if (GUI.Button(new Rect(window.width - 110, 290, 90, 17), "Update"))
            {
                try
                {
                    MapManager.UpdateMapByLevel(int.Parse(level), locked.ToString(), int.Parse(star), int.Parse(highScore), int.Parse(targetScore), float.Parse(time));
                    warningMessage = "Update successfully";
                    RestartValue();
                }
                catch (System.Exception ex)
                {
                    warningMessage = ex.Message;
                }
                return;
            }
        }


        GUI.Box(new Rect(10, 320, window.width - 20, window.height - 140), "");

        if (PlayerPrefs.HasKey(MapManager.NUMBER_OF_LEVEL) && PlayerPrefs.GetInt(MapManager.NUMBER_OF_LEVEL, 0) > 0)
        {
            scrollPosition = GUI.BeginScrollView(new Rect(0, 320, window.width - 10, window.height - 140), scrollPosition, new Rect(0, 0, window.width - 50, 20 + (25 * PlayerPrefs.GetInt(MapManager.NUMBER_OF_LEVEL) * 6)));
            for (int i = 1; i <= PlayerPrefs.GetInt(MapManager.NUMBER_OF_LEVEL); i++)
            {
                GUI.Label(new Rect(20, 10 + (i * 25), 110, 17), "Level : ");
                GUI.Label(new Rect(70, 10 + (i * 25), 50, 17), PlayerPrefs.GetInt(MapManager.LEVEL_ + i).ToString());

                GUI.Label(new Rect(140, 10 + (i * 25), 110, 17), "Locked : ");
                EditorGUI.TextField(new Rect(210, 10 + (i * 25), 50, 17), PlayerPrefs.GetString(MapManager.LOCKED_ + i));

                GUI.Label(new Rect(280, 10 + (i * 25), 110, 17), "Star : ");
                EditorGUI.TextField(new Rect(330, 10 + (i * 25), 50, 17), PlayerPrefs.GetInt(MapManager.STARS_ + i).ToString());

                GUI.Label(new Rect(400, 10 + (i * 25), 110, 17), "High Score : ");
                EditorGUI.TextField(new Rect(500, 10 + (i * 25), 120, 17), PlayerPrefs.GetInt(MapManager.HIGH_SCORE_ + i).ToString());

                GUI.Label(new Rect(640, 10 + (i * 25), 110, 17), "Target Score : ");
                EditorGUI.TextField(new Rect(740, 10 + (i * 25), 120, 17), PlayerPrefs.GetInt(MapManager.TARGET_ + i).ToString());

                GUI.Label(new Rect(880, 10 + (i * 25), 110, 17), "Time : ");
                EditorGUI.TextField(new Rect(930, 10 + (i * 25), 120, 17), PlayerPrefs.GetFloat(MapManager.GAME_TIME_ + i).ToString());

                if (GUI.Button(new Rect(window.width - 80, 10 + (i * 25), 20, 17), "U"))
                {
                    level = PlayerPrefs.GetInt(MapManager.LEVEL_ + i).ToString();
                    locked = PlayerPrefs.GetString(MapManager.LOCKED_ + i);
                    star = PlayerPrefs.GetInt(MapManager.STARS_ + i).ToString();
                    highScore = PlayerPrefs.GetInt(MapManager.HIGH_SCORE_ + i).ToString();
                    targetScore = PlayerPrefs.GetInt(MapManager.TARGET_ + i).ToString();
                    time = PlayerPrefs.GetFloat(MapManager.GAME_TIME_ + i).ToString();
                    isUpdate = true;
                }

                if (GUI.Button(new Rect(window.width - 50, 10 + (i * 25), 20, 17), "X"))
                {
                    if (i != PlayerPrefs.GetInt(MapManager.NUMBER_OF_LEVEL))
                    {
                        warningMessage = "Delete last element in list";
                    }
                    else
                    {
                        MapManager.DeleteMapByLevel(i);
                        warningMessage = "Deleted successfully";
                    }
                    return;
                }
            }
            GUI.EndScrollView();
        }
    }
}
