﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GoogleMobileAds.Api;

public class GoogleMobileAdHolder : MonoBehaviour {

    private static GoogleMobileAdHolder _instance = null;

    [SerializeField]
    private GoogleMobileAdAccount googleMobileAdAccountData;

    private RewardBasedVideoAd rewardBasedVideo;
    private InterstitialAd interstitial;
    private BannerView bannerView;

    private bool getReward;
    private bool isRewardAd = false;

    public static GoogleMobileAdHolder Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start() {

        string appId = "unexpected_platform";
#if UNITY_ANDROID
        appId = googleMobileAdAccountData.android.appId;
#elif UNITY_IOS
        appId = googleMobileAdAccountData.ios.appId;
#endif
        MobileAds.Initialize(appId);

        this.rewardBasedVideo = RewardBasedVideoAd.Instance;

        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;

        RequestBannerAd();
        RequestRewardBasedVideo();
        RequestInterstitialAd();
    }

    private void RequestBannerAd()
    {
        Debug.LogError("RequestBannerAd");

#if UNITY_ANDROID
        string adUnitId = googleMobileAdAccountData.android.adBannerUnitId;
#elif UNITY_IOS
        string adUnitId = googleMobileAdAccountData.ios.adBannerUnitId;
#else
        string adUnitId = "unexpected_platform";
#endif

        if (this.bannerView != null)
        {
            this.bannerView.Destroy();
        }

        //AdSize adSize = new AdSize(320, 50);
        this.bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);

        bannerView.OnAdLoaded += HandleOnBannerAdLoaded;
        bannerView.OnAdFailedToLoad += HandleOnBannerAdFailedToLoad;
        bannerView.OnAdOpening += HandleOnBannerAdOpened;
        bannerView.OnAdClosed += HandleOnBannerAdClosed;

        AdRequest request = new AdRequest.Builder()
#if UNITY_IOS
            //.AddTestDevice("0553565d38ff52a25063cb58a42a615f")
            .AddTestDevice("23448e02711118d4df682b6f6d57ba10")
#elif UNITY_ANDROID
            //.AddTestDevice("264800B5B9D9F2473B49D5B6DDA6DA61")
            .AddTestDevice("2855BB130F84EFBC86C90286D881CF32")
#endif
            .Build();

        bannerView.LoadAd(request);
    }

    private void RequestInterstitialAd() 
    {
        Debug.LogError("RequestInterstitialAd");
#if UNITY_ANDROID
        string adUnitId = googleMobileAdAccountData.android.adInterstitialUnitId;
#elif UNITY_IOS
        string adUnitId = googleMobileAdAccountData.ios.adInterstitialUnitId;
#else
        string adUnitId = "unexpected_platform";
#endif

        this.interstitial = new InterstitialAd(adUnitId);
        interstitial.OnAdOpening += HandleOnAdOpened;
        interstitial.OnAdClosed += HandleOnAdClosed;
        interstitial.OnAdLoaded += HandleOnAdLoaded;

        AdRequest request = new AdRequest.Builder()
#if UNITY_IOS
            //.AddTestDevice("0553565d38ff52a25063cb58a42a615f")
            .AddTestDevice("23448e02711118d4df682b6f6d57ba10")
#elif UNITY_ANDROID
            //.AddTestDevice("264800B5B9D9F2473B49D5B6DDA6DA61")
            .AddTestDevice("2855BB130F84EFBC86C90286D881CF32")
#endif
            .Build();
        this.interstitial.LoadAd(request);
    }

    private void RequestRewardBasedVideo()
    {
        Debug.LogError("request reward");
#if UNITY_ANDROID
        string adUnitId = googleMobileAdAccountData.android.adRewardVideoUnitId;
#elif UNITY_IOS
        string adUnitId = googleMobileAdAccountData.ios.adRewardVideoUnitId;
#else
            string adUnitId = "unexpected_platform";
#endif

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder()
#if UNITY_IOS
            //.AddTestDevice("0553565d38ff52a25063cb58a42a615f")
            //.AddTestDevice("23448e02711118d4df682b6f6d57ba10")
            //.AddTestDevice("dfe1e11f09a522d5857f4e5e0bb5e63d")
#elif UNITY_ANDROID
            //.AddTestDevice("264800B5B9D9F2473B49D5B6DDA6DA61")
            .AddTestDevice("2855BB130F84EFBC86C90286D881CF32")
#endif
            .Build();
        // Load the rewarded video ad with the request.
        this.rewardBasedVideo.LoadAd(request, adUnitId);
    }

    private void OnDestroy()
    {
        rewardBasedVideo.OnAdFailedToLoad -= HandleRewardBasedVideoFailedToLoad;
        rewardBasedVideo.OnAdOpening -= HandleRewardBasedVideoOpened;
        rewardBasedVideo.OnAdClosed -= HandleRewardBasedVideoClosed;
        rewardBasedVideo.OnAdRewarded -= HandleRewardBasedVideoRewarded;
        rewardBasedVideo.OnAdLoaded -= HandleRewardBasedVideoLoaded;

        bannerView.OnAdLoaded -= HandleOnBannerAdLoaded;
        bannerView.OnAdFailedToLoad -= HandleOnBannerAdFailedToLoad;
        bannerView.OnAdOpening -= HandleOnBannerAdOpened;
        bannerView.OnAdClosed -= HandleOnBannerAdClosed;

        DestroyInterstitial();
    }

    private void DestroyInterstitial()
    {
        interstitial.OnAdLoaded -= HandleOnAdLoaded;
        interstitial.OnAdOpening -= HandleOnAdOpened;
        interstitial.OnAdClosed -= HandleOnAdClosed;
        interstitial.Destroy(); 
    }

    public void ShowAd()
    {
        //isRewardAd = true;

        if (rewardBasedVideo.IsLoaded())
        {
            Debug.LogError("video loaded");
            rewardBasedVideo.Show();
        }
    }

    public void ShowInterstitialAd()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
    }

    #region Reward Ad Handlers

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        Debug.LogError("HandleRewardBasedVideoLoaded event received");
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        Music.music.audiosource.Pause();
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        DestroyInterstitial();
        RequestInterstitialAd();
        Music.music.audiosource.UnPause();
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        Debug.LogError("Interstitial finished loading");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.LogError("failed to load reward video");
        RequestRewardBasedVideo();
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        Sound.sound.Pause();
        Music.music.audiosource.Pause();
        getReward = false;
        //Debug.LogError("getReward video opened: " + getReward);
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        //Debug.LogError("getReward video closed: " + getReward);
        RequestRewardBasedVideo();

        /*if (isRewardAd)
        {*/
            if (getReward)
            {
                Timer.get.AdReward();
                Sound.sound.Stop();
            }
            else
            {
                Timer.get.ReturnToResult();
                Sound.sound.UnPause();
            }
        //}
        
        Music.music.audiosource.UnPause();
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        getReward = true;
        //Debug.LogError("will get reward");
    }

    #endregion

    #region Banner Ad Handlers

    public void HandleOnBannerAdLoaded(object sender, EventArgs args)
    {
        Debug.LogError("HandleAdLoaded event received");
    }

    public void HandleOnBannerAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.LogError("HandleFailedToReceiveAd event received with message: "
                            + args.Message);
        RequestBannerAd();
    }

    public void HandleOnBannerAdOpened(object sender, EventArgs args)
    {
        Debug.LogError("HandleAdOpened event received");
    }

    public void HandleOnBannerAdClosed(object sender, EventArgs args)
    {
        Debug.LogError("HandleAdClosed event received");
    }

    #endregion

}

[Serializable]
public class GoogleMobileAdId
{
    [SerializeField]
    internal string appId;
    [SerializeField]
    internal string adRewardVideoUnitId;
    [SerializeField]
    internal string adInterstitialUnitId;
    [SerializeField]
    internal string adBannerUnitId;
}

[Serializable]
public class GoogleMobileAdAccount
{
    [SerializeField]
    internal GoogleMobileAdId android;
    [SerializeField]
    internal GoogleMobileAdId ios;
}
