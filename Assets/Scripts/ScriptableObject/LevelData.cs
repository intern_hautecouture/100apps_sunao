﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class LevelData : ScriptableObject
{
    public int levelNumber;
    public List<int> levelTimes = new List<int>();
    public List<int> levelTargetScore = new List<int>();

    public void Reset()
    {
        levelTimes.Clear();
        levelTargetScore.Clear();
    }


}
