﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.iOS;

public class IphoneXResolution : MonoBehaviour {


	[SerializeField]
	private Vector2 pos;

	void Awake(){

#if !UNITY_EDITOR
#if UNITY_IOS
		var device = Device.generation;
        //Debug.LogError(device.ToString());

		if (device.ToString().Contains ("iPhoneX") || device.ToString().Contains("iPhoneUnknown")) {
            Debug.LogError(device.ToString());
            Debug.LogError("belongs to iphone X family");
			this.gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (pos.x, pos.y);
		}
#endif
#else
        //print(DeviceGeneration.iPhoneX);
        if ((Screen.width == 1125 && Screen.height == 2436) || (Screen.width == 828 && Screen.height == 1792))
        {
		this.gameObject.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (pos.x, pos.y);
		}
#endif
	}
}
