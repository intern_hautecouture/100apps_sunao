﻿using UnityEngine;
using System.Collections;

public class MultiResolution : MonoBehaviour
{
    // scale screen with base resolution for mobile width : 720 and hight 1280

#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8
    private Transform m_tranform;
    private static float BASE_WIDTH = 720f;
    private static float BASE_HEIGHT = 1280f;
    private float baseRatio;
    private float percentScale;

    void Start()
    {
        m_tranform = transform;
        setScale();
    }
    void setScale()
    {
        //print("screen width: " + Screen.width);
        //print("screen height: " + Screen.height);


        baseRatio = (float)BASE_WIDTH / BASE_HEIGHT * Screen.height;
        percentScale = Screen.width / baseRatio;
        if (percentScale > 1)
        {
            m_tranform.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            m_tranform.localScale = new Vector3(percentScale, 1, 1);
        }
        //print(percentScale);
    }

    public float GetPercentScale()
    {
        return percentScale;
    }
#endif

}
