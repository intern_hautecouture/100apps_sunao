﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceCheck : MonoBehaviour {


	[SerializeField]
	private GameObject ScreenMenuCanvas;

	[SerializeField]
	private GameObject ScreenMenuForipadCanvas;

    [SerializeField]
    private GameObject ScreenMenuCanvasAndroid;

	void Awake(){
		Check ();
	}

	void Check(){

        //Debug.Log (Screen.width + ";" + Screen.height);

#if !UNITY_EDITOR
#if UNITY_IOS

		if(SystemInfo.deviceModel.Contains("iPad")){
			ScreenMenuForipadCanvas.SetActive (true);
		}
		else{
			ScreenMenuCanvas.SetActive (true);
		}
#endif
#if UNITY_ANDROID
        float width = Screen.width;
        float height = Screen.height;
        //double res = System.Math.Round((width / height), 2);
        float res = width / height;
        Debug.LogError("res " + res);

        if (width == 600 && height == 1024)
        {
            ScreenMenuCanvasAndroid.SetActive(true);
            var top = ScreenMenuCanvasAndroid.transform.Find("top");
            top.localPosition += new Vector3(0, -20, 0);
        }
        else if (Camera.main.aspect == 0.6666667f)
        {
            ScreenMenuCanvasAndroid.SetActive(true);
            var top = ScreenMenuCanvasAndroid.transform.Find("top");
            top.localPosition += new Vector3(0, 20, 0);
            var img = ScreenMenuCanvasAndroid.transform.Find("Image");
            img.localPosition += new Vector3(0, 20, 0);
        }
        else if (res < 0.6)
            ScreenMenuCanvas.SetActive(true);
        else
            ScreenMenuCanvasAndroid.SetActive(true);
#endif
#else

        if (Screen.width == 768 && Screen.height == 1024){
			ScreenMenuForipadCanvas.SetActive (true);
		}
		else{
            float width = Screen.width;
            float height = Screen.height;
            //double res = System.Math.Round((width / height), 2);
            float res = width / height;
            print(Camera.main.aspect);
            if (width == 600 && height == 1024)
            {
                ScreenMenuCanvasAndroid.SetActive(true);
                var top = ScreenMenuCanvasAndroid.transform.Find("top");
                top.localPosition += new Vector3(0, -20, 0);
            }
            else if (Camera.main.aspect == 0.6666667f)
            {
                ScreenMenuCanvasAndroid.SetActive(true);
                var top = ScreenMenuCanvasAndroid.transform.Find("top");
                top.localPosition += new Vector3(0, 20, 0);
                var img = ScreenMenuCanvasAndroid.transform.Find("Image");
                img.localPosition += new Vector3(0, 20, 0);
            }
            else if (res < 0.6)
                ScreenMenuCanvas.SetActive(true);
            else
                ScreenMenuCanvasAndroid.SetActive(true);
            print("res: " + res);
            //ScreenMenuCanvas.SetActive (true);
        }

#endif


    }

}
