﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ButtonAction : MonoBehaviour {

	ButtonController buttonController;

	void Awake(){

        buttonController = GameObject.Find("ButtonController").GetComponent<ButtonController>();
        GetComponent<Button>().onClick.AddListener(()=> OnClick());
	}

	public void OnClick(){
		buttonController.ButtonAction(gameObject);

        if (gameObject.GetComponent<Animation>())
        {
            gameObject.GetComponent<Animation>().Play();
        }
	}


}
