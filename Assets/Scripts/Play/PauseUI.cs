﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour
{

    public Image[] SoundSprite;

    public Animation Anim;

    void Start()
    {
        if (Timer.GameState == 1)
            defautButton();
    }
    void defautButton()
    {
        int soundstate = PlayerPrefs.GetInt(ButtonController.SOUNDSTATE);
        int musicstate = PlayerPrefs.GetInt(ButtonController.MUSICSTATE);

        if (soundstate == 0)
        {
			SoundSprite[0].sprite = ButtonController.m_ButtonController.ButtonSprite[4];
            Sound.isSound = true;
        }
        else
        {
            SoundSprite[0].sprite = ButtonController.m_ButtonController.ButtonSprite[5];
            Sound.isSound = false;
        }

        if (musicstate == 0)
        {
            SoundSprite[1].sprite = ButtonController.m_ButtonController.ButtonSprite[6];
            Music.music.MusicON();
        }
        else
        {
            SoundSprite[1].sprite = ButtonController.m_ButtonController.ButtonSprite[7];
            Music.music.MusicOFF();
        }
    }

    public void PanelOpen()
    {
        
        Anim.Play();
    }

}
