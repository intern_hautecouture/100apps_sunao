﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    // GameState : 0 - playing, 1 - Pause, 2 - Win, 3 - Lose

    public static Timer get;
    public static float time;
    public static bool tick;
    public static int GameState = -1;

    public GameObject Warning;
    public GameObject PauseUI;
    public GameObject LoseUI;
    public GameObject WinUI;
    public GameObject GameOverAdCanvas;
    public GameObject Not3StarsAdCanvas;

	public Slider timebar;
    private float Maptime;

    private bool isWarn;
    private bool showAdCanvas = true;

    public static bool shownInterstitial = false;

    void Start()
    {
        get = this;
        Maptime = time;
		timebar.maxValue = Maptime;
		timebar.value = timebar.maxValue;
        //print("timer time: " + Maptime);
        tick = true;
        GameState = 0;

		StartCoroutine (Time());
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ActionKeyPress(GameState);
        }
    }


	IEnumerator Time(){

		while (tick) {
			yield return new WaitForSeconds (0.1f);
			Timebarprocess ();       
		}
	}

    void GameResult()
    {
        Debug.LogError("shownInterstitial: " + shownInterstitial);

        if (!shownInterstitial)
        {
            showAdCanvas = false;
            shownInterstitial = true;
            //Debug.LogError("shownInterstitial: " + shownInterstitial);
            Invoke("ShowInterstitialAd", Random.Range(0.5f, 0.8f));
        }
        else
        {
            shownInterstitial = false;
        }

        if (PlayerInfo.Info.Score < PlayerInfo.TargetScore)
        {
            Lose();
        }
        else
        {
            Win();
        }
    }

    void ShowInterstitialAd()
    {
        GoogleMobileAdHolder.Instance.ShowInterstitialAd();
    }

    void Lose()
    {
        GameState = 2;
        Sound.sound.lose();
        tick = false;
        LoseUI.SetActive(true);
        //LoseUI.GetComponent<PauseUI>().Anim.enabled = true;
        //LoseUI.GetComponent<PauseUI>().Anim.Play("PanelMove");
        if (showAdCanvas)
            Invoke("ShowGameOverAdCanvas", 0.7f);

    }

    void Win()
    {
        GameState = 3;
        tick = false;
        Sound.sound.win();
        print("score: " + PlayerInfo.Info.Score);
        if (PlayerInfo.Info.Score > PlayerInfo.MapPlayer.HightScore)
            PlayerInfo.MapPlayer.HightScore = PlayerInfo.Info.Score;

        int star = GetStar((int)PlayerInfo.Info.Score);

        PlayerInfo.MapPlayer.LastStars = star;
        //print("last stars: " + PlayerInfo.MapPlayer.LastStars);
        if (star > PlayerInfo.MapPlayer.Stars)
            PlayerInfo.MapPlayer.Stars = star;
        //print("star: " + star);
        //save info current level and unlock the next level
        MapManager.UpdateMapByLevel(PlayerInfo.MapPlayer.Level, PlayerInfo.MapPlayer.Stars, PlayerInfo.MapPlayer.HightScore);

        WinUI.SetActive(true);
        WinUI.GetComponent<WinUI>().Anim.enabled = true;
        WinUI.GetComponent<WinUI>().Anim.Play("PanelMove");
        if (star < 3)
        {
            if (showAdCanvas)
            {
                WinUI.GetComponent<WinUI>().TurnOffButtons();
                Invoke("ShowWinAdCanvas", 0.7f);
            }
        }

    }
    int GetStar(int score)
    {
        int level = PlayerInfo.MapPlayer.Level;
        int twoStarMinValue, threeStarMinValue;

        if (level < 51)
        {
            twoStarMinValue = 1000;
            threeStarMinValue = 1500;
        }
        else if (level >= 51 && level < 76)
        {
            twoStarMinValue = 1000;
            threeStarMinValue = 2000;
        }
        else
        {
            twoStarMinValue = 1000;
            threeStarMinValue = 2000;
        }

        if (score >= PlayerInfo.TargetScore + threeStarMinValue)
            return 3;
        else if (score >= PlayerInfo.TargetScore + twoStarMinValue)
            return 2;
        return 1;
    }

    void ActionKeyPress(int gamestate)
    {
        switch (gamestate)
        {
            case 0:
                Pause();
                break;
            case 1:
                Resume();
                break;
            default:
                SceneManager.LoadScene("MapsScene");
                break;
        }
    }

    void Timebarprocess()
    {
		timebar.value -= 0.1f;

		if (timebar.value <= 0)
		{
			tick = false;
			DisableWarning();
			GameResult();
		}

		if (!isWarn && timebar.value <= 10)
        {
            Sound.sound.waring();
            EnableWarning();
            isWarn = true;
        }
    }

    void EnableWarning()
    {
        Warning.GetComponent<Animation>().enabled = true;
        Warning.GetComponent<Animation>().Play("Warning");
        Warning.SetActive(true);
    }
    void DisableWarning()
    {
        Warning.GetComponent<Animation>().enabled = false;
        Warning.SetActive(false);
    }

    public void Pause()
    {
        tick = false;
        PauseUI.SetActive(true);
        GoogleMobileAdHolder.Instance.ShowInterstitialAd();
        //PauseUI.GetComponent<PauseUI>().Anim.enabled = true;
        //PauseUI.GetComponent<PauseUI>().Anim.Play("PanelMove");
        GameState = 1;

    }

    void ShowGameOverAdCanvas()
    {
        print("show canvas");
        GameOverAdCanvas.SetActive(true);
    }

    //this is shown when the player doesn't get 3 stars
    void ShowWinAdCanvas()
    {
        Not3StarsAdCanvas.SetActive(true);
    }

    public void Resume()
    {
        tick = true;
        //PauseUI.GetComponent<PauseUI>().Anim.enabled = false;
        PauseUI.SetActive(false);
        GameState = 0;
        StartCoroutine(Time());
    }

    public void Menu()
    {
        //nendbanner.Hide();
    }

    public void AdReward()
    {
        tick = true;
        isWarn = false;
        GameState = 0;
        //print("before set active lose ui");
        LoseUI.SetActive(false);
        //print("lose ui set to in active");
        //LoseUI.GetComponent<PauseUI>().Anim.enabled = true;
        WinUI.GetComponent<WinUI>().TurnOnButtons();
        timebar.value = Maptime / 2;
        DisableWarning();
        StartCoroutine(Time());

        //Debug.Log("end of ad reward");
    }

    public void ReturnToResult()
    {
        if (GameState==3)
        {
            WinUI.SetActive(true);
            WinUI.GetComponent<WinUI>().TurnOnButtons();
        }
    }
}
