﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class FruitSpawner : MonoBehaviour
{
    public static FruitSpawner Spawner;

    public List<int> FruitIndex;

    public GameObject[,] FruitGrid;

    public List<GameObject>[] RespawnFruitList;

    public GameObject ThrowSelect;

    public GameObject FruitPrefab;

    public Sprite[] FruitSprite;

    public Sprite[] FruitSpriteHold;

    public GameObject FruitParent;

    public static float PosY = 4.5f;

    public static float CollumnSpace = 0.654f;

    public static float RowSpace = 0.651f;

    private GameObject FruitTemp;

    public static Vector2 rootPos = new Vector2(-1.962f, -1.933f);
    private static bool reescaled = false;

    private Fruit FruitScript;

    public int[] idxpos;

    public GameObject board;
    private MultiResolution multiRes;

    void Awake()
    {
        FruitIndex = new List<int>();
        Spawner = this;
        RespawnFruitList = new List<GameObject>[7];

    }
    void Start()
    {
        listspawnde();
        multiRes = GameObject.Find("Screen").GetComponent<MultiResolution>();
        //PosY *= multiRes.GetPercentScale();

        var boardScale = board.transform.localScale;
        if(multiRes.GetPercentScale() > 1)
        {
            //CollumnSpace /= multiRes.GetPercentScale();
            //RowSpace *= multiRes.GetPercentScale();

            //rootPos.y *= multiRes.GetPercentScale();
        }
        else
        {
            if (!reescaled)
            {
                rootPos.y *= multiRes.GetPercentScale();
                RowSpace *= multiRes.GetPercentScale();
                reescaled = true;
            }
            boardScale.y *= multiRes.GetPercentScale();

        }


        board.transform.localScale = boardScale;

        FruitGrid = new GameObject[7, 7];

        FruitIndex = RandomFruitIndex();

        FruitGridCreate();
    }

    void listspawnde()
    {
        for (int i = 0; i < 7; i++)
        {
            RespawnFruitList[i] = new List<GameObject>();
        }
    }
    void FruitGridCreate()
    {
        for (int x = 0; x < 7; x++)
            for (int y = 0; y < 7; y++)
            {
                FruitIns(x, y);
            }
    }

    void FruitIns(int x, int y)
    {
        int r = Random.Range(0, 6);
        r = FruitIndex[r];
        Vector3 pos = new Vector3(rootPos.x + x * CollumnSpace, rootPos.y + y * RowSpace);
        SpawnFruit(pos, r, 0, new Vector2(x, y));
    }

    List<int> RandomFruitIndex()
    {
        int numberoffruit = 6;
        if (PlayerInfo.MapPlayer.Level >= 76)
        {
            numberoffruit = 8;
        }
        else if (PlayerInfo.MapPlayer.Level >= 51)
        {
            numberoffruit = 7;
        }

        List<int> listtmp = new List<int>();
        List<int> tmp = new List<int>();
        for (int i = 0; i < 8; i++)
        {
            listtmp.Add(i);
        }

        for (int i = 0; i < numberoffruit; i++)
        {
            tmp.Add(listtmp[Random.Range(0, listtmp.Count)]);
            listtmp.Remove(tmp[i]);
        }

        return tmp;
    }

    public void SpawnFruitPower(GameObject obj, int Power)
    {

        FruitScript = obj.GetComponent<Fruit>();
        FruitScript.SetTag("fruitSelected");
        ThrowSelect = obj;
        obj.transform.localPosition = new Vector3(rootPos.x + FruitScript.FruitPosition.x * CollumnSpace, rootPos.y + FruitScript.FruitPosition.y * RowSpace, -0.05f);
        FruitGrid[(int)FruitScript.FruitPosition.x, (int)FruitScript.FruitPosition.y] = obj;
        EffectSpawner.Spawn.AddPower(obj);
        if (Power == 4)
        {
            FruitScript.SetSpritePower4();
            FruitScript.FruitType = 8;
        }
        FruitScript.FruitPower = Power;

        FruitScript.DropEffect();
        EffectSpawner.Spawn.SpawnEffectPower(Power, obj);

        FruitScript.isPowerUp = false;
    }


    GameObject SpawnFruit(Vector3 position, int type, int Power, Vector2 FruiPosition)
    {
        FruitTemp = (GameObject)Instantiate(FruitPrefab);
        FruitScript = FruitTemp.GetComponent<Fruit>();
        FruitTemp.transform.parent = FruitParent.transform;
        FruitTemp.transform.localPosition = position;
        if (multiRes.GetPercentScale() < 1)
            FruitTemp.transform.localScale = new Vector3(0.19f, 0.19f * multiRes.GetPercentScale(), 0.19f);
        else
            FruitTemp.transform.localScale = new Vector3(0.19f, 0.19f, 0.19f);

        FruitTemp.transform.Find("render").GetComponent<SpriteRenderer>().sprite = FruitSprite[type];
        FruitTemp.transform.Find("Selected").GetComponent<SpriteRenderer>().sprite = FruitSpriteHold[type];
        FruitScript.FruitType = type;
        FruitScript.FruitPower = Power;
        FruitScript.FruitPosition = FruiPosition;
        FruitGrid[(int)FruiPosition.x, (int)FruiPosition.y] = FruitTemp;
        return FruitTemp;
    }

    public void AddFruit()
    {
        int x = 0;
        int y = 0;
        for (int i = 0; i < 7; i++)
        {
            if (RespawnFruitList[i].Count > 0)
            {
                if (RespawnFruitList[i].Count >= y)
                {
                    y = RespawnFruitList[i].Count;
                    x = i;
                }
                for (int j = 0; j < RespawnFruitList[i].Count; j++)
                {
                    RemakeFruit(RespawnFruitList[i][j], i);
                }
            }
        }
        RespawnFruitList[x][y - 1].GetComponent<Fruit>().Move.FLAG = true;
        for (int i = 0; i < 7; i++)
        {
            RespawnFruitList[i].Clear();
        }
    }

    GameObject RemakeFruit(GameObject obj, int x)
    {
        int r = Random.Range(0, FruitIndex.Count);
        r = FruitIndex[r];
        FruitScript = obj.GetComponent<Fruit>();
        obj.transform.Find("render").GetComponent<SpriteRenderer>().sprite = FruitSprite[r];
        obj.transform.Find("Selected").GetComponent<SpriteRenderer>().sprite = FruitSpriteHold[r];
        FruitScript.FruitType = r;
        FruitScript.FruitPower = 0;
        FruitScript.FruitPosition.y = 7;
        int y = (int)FruitScript.FruitPositionFinder(x).y;
        FruitScript.FruitPosition = new Vector2(x, y);
        FruitGrid[x, y] = obj;
        FruitScript.Move.pos = rootPos.y + FruitScript.FruitPosition.y * RowSpace;
        FruitScript.Move.enabled = true;

        return obj;

    }
}
