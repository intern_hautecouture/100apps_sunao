﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class ButtonAnimation : MonoBehaviour
{

    GameObject tmp;
    void OnMouseDown()
    {
        Sound.sound.click();
        Animation anim = GetComponent<Animation>();
        anim.enabled = true;
        anim.Play("ButtonPress");
    }

    void OnMouseUp()
    {
        Animation anim = GetComponent<Animation>();
        anim.enabled = true;
        anim.Play("ButtonUp");
        tmp = TouchChecker(Input.mousePosition);
        if (tmp != null && tmp == this.gameObject)
        {
            ButtonActionController.p_pause();
        }
    }

    GameObject TouchChecker(Vector3 mouseposition)
    {
        Vector3 wp = Camera.main.ScreenToWorldPoint(mouseposition);
        Vector2 touchPos = new Vector2(wp.x, wp.y);
        if (Physics2D.OverlapPoint(touchPos))
        {
            return Physics2D.OverlapPoint(touchPos).gameObject;
        }
        return null;
    }

}
