﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NativeAdUI : MonoBehaviour {

    public GameObject panel;
    public Text adTitleText;
    public Text adDescriptionText;
    public Text adSponsorText;
    public RawImage adImage;

}
