﻿using UnityEngine;
using System.Collections;

public class PlayerInfo : MonoBehaviour
{

    public static PlayerInfo Info = new PlayerInfo();
    public static Player MapPlayer;
    public static int TargetScore;
    public static float AdsShowTime = 120f;
    public int Score = 0;
    public float MapTime;
    const int ScoreRank = 20;

    void Awake()
    {
        MapTime = MapPlayer.GameTime;
        TargetScore = MapPlayer.Target;
        //print("maptime: " + MapTime);
        Timer.time = MapTime;
    }

    /// <summary>
    /// set infomation for current level : level, target score, score
    /// </summary>
    void Start()
    {
        Score = 0;
        Sound.sound.startgame();
		EffectSpawner.Spawn.FindText ();
        EffectSpawner.Spawn.SetTextLevel();
        EffectSpawner.Spawn.SetTextTargetScore();
        EffectSpawner.Spawn.SetTextScore();
    }
}
