﻿using UnityEngine;
using System.Collections;

public class AnimationDisable : MonoBehaviour {
    public Animation anim;

    /// <summary>
    /// disable animation
    /// </summary>
    public void Disable()
    {
        anim.enabled = false;
    }
}
