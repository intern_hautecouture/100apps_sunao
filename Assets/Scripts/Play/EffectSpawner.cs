﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EffectSpawner : MonoBehaviour
{
    public static EffectSpawner Spawn;

    public GameObject Screen;

    public GameObject[] EffectPrefabs;

    public GameObject[] ScoreEffectPrefabs;

    public Sprite[] number;

    public GameObject WinVFX;

    private GameObject tmp;

	private Text LevelText;

	private Text TargetScoreText;

	private Text ScoreText;

    private float BLAST_TIME = 0.5f;

    private float BAST_ADD_POWER = 0.567f;

    private float BAST_PARK_TIME = 0.371f;

    private float FIRE_STAR_TiME = 1;

    private float SCORE_NUMBER_TIME = 0.667f;

    void Awake()
    {
        Spawn = this;
    }
		
    public void Blast(GameObject obj)
    {
        Spawner(obj, EffectPrefabs[0], BLAST_TIME);
    }

    public void AddPower(GameObject obj)
    {
        Spawner(obj, EffectPrefabs[1], BAST_ADD_POWER);
    }

    public void BlastPark(GameObject obj)
    {
        Spawner(obj, EffectPrefabs[2], BAST_PARK_TIME);
    }

    public void SpawnEffectPower(int power, GameObject Fruit)
    {
        switch (power)
        {
            case 1:
                PowerType1(Fruit);
                break;
            case 2:
                PowerType2(Fruit);
                break;
            case 3:
                PowerType1(Fruit);
                PowerType2(Fruit);
                break;
        }
        Light(Fruit);

    }

    public void DestroyEffectPower(GameObject parent, int power, Vector3 localpos)
    {
        switch (power)
        {
            case 1:
                firestar(parent, localpos + new Vector3(0, 0, -0.02f), Vector3.zero);
                break;
            case 2:
                firestar(parent, localpos + new Vector3(0, 0, -0.02f), new Vector3(0, 0, 90));
                break;
            case 3:
                firestar(parent, localpos + new Vector3(0, 0, -0.02f), Vector3.zero);
                firestar(parent, localpos + new Vector3(0, 0, -0.02f), new Vector3(0, 0, 90));
                break;
        }
    }

    public void ScoreSpawn(GameObject obj, int score)
    {
        Score(obj, score, SCORE_NUMBER_TIME);
    }


	public void FindText(){
		LevelText = GameObject.Find ("NumberLevel").GetComponent<Text>();
		ScoreText = GameObject.Find ("NumberScore").GetComponent<Text>();
		TargetScoreText = GameObject.Find ("NumberTarget").GetComponent<Text>();
	}

    private void Score(GameObject obj, int score, float TIME)
    {
        if (score >= 100)
        {
            tmp = (GameObject)Instantiate(ScoreEffectPrefabs[2], obj.transform.position, ScoreEffectPrefabs[2].transform.rotation);
            tmp.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = number[score / 100];
            tmp.transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = number[score % 100 / 10];

        }
        else if (score >= 10)
        {
            tmp = (GameObject)Instantiate(ScoreEffectPrefabs[1], obj.transform.position, ScoreEffectPrefabs[2].transform.rotation);
            tmp.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = number[score % 100 / 10];
        }
        else
        {
            tmp = (GameObject)Instantiate(ScoreEffectPrefabs[0], obj.transform.position, ScoreEffectPrefabs[2].transform.rotation);
        }
        if (Timer.GameState == 0)
        {
            PlayerInfo.Info.Score += score;
            if (PlayerInfo.Info.Score < 0)
                PlayerInfo.Info.Score = 0;
        }

        tmp.transform.parent = Screen.transform;
        tmp.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y, -3.0f);

        Destroy(tmp, TIME);
    }

    private void firestar(GameObject parent, Vector3 pos, Vector3 rotation)
    {
        tmp = Spawner(parent, EffectPrefabs[5], rotation);
        tmp.transform.position = pos;
        Destroy(tmp, FIRE_STAR_TiME);
    }

    private void PowerType1(GameObject obj)
    {
        Spawner(obj, EffectPrefabs[3], Vector3.zero);

    }
    private void PowerType2(GameObject obj)
    {
        Spawner(obj, EffectPrefabs[3], new Vector3(0, 0, 90));
    }


    GameObject Spawner(GameObject obj, GameObject prefab, Vector3 rotation)
    {
        tmp = (GameObject)Instantiate(prefab, obj.transform.position, prefab.transform.rotation);
        tmp.transform.parent = obj.transform;
        tmp.transform.localScale = prefab.transform.localScale;
        tmp.transform.localPosition = new Vector3(0, 0, -0.03f);
        tmp.transform.rotation = Quaternion.Euler(rotation);
        return tmp;
    }

    void Light(GameObject obj)
    {
        tmp = (GameObject)Instantiate(EffectPrefabs[4]);
        tmp.transform.parent = obj.transform;
        tmp.transform.localPosition = new Vector3(0, 0, -0.02f);
    }

    GameObject Spawner(GameObject obj, GameObject prefab, float TIME)
    {
        tmp = (GameObject)Instantiate(prefab, obj.transform.position, prefab.transform.rotation);
        tmp.transform.parent = Screen.transform;
        tmp.transform.localScale = prefab.transform.localScale;

        Destroy(tmp, TIME);
        return null;
    }

    public void SetTextLevel()
    {
        int lv = PlayerInfo.MapPlayer.Level;
        LevelText.text = lv.ToString();
    }

    public void SetTextTargetScore()
    {
        int t = PlayerInfo.TargetScore;
        TargetScoreText.text = t.ToString();
    }

    public void SetTextScore()
    {
        int t = (int)PlayerInfo.Info.Score;
        //Debug.Log(t);
        ScoreText.text = t.ToString();
    }

    public void HeartVFX(GameObject obj)
    {
        var temp = Instantiate(WinVFX, obj.transform);

        temp.transform.parent = Screen.transform;

        Debug.Log("heart");
        Destroy(temp, 1.5f);
    }
}
