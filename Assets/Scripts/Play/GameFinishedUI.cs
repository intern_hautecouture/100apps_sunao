﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFinishedUI : MonoBehaviour {

    public GameObject uiToDeactivate;

    public WinUI winUI;

	public void WatchAdForExtraTimeToComplete()
    {
        //MaioAd.Instance.ShowAd();
        gameObject.SetActive(false);

        GoogleMobileAdHolder.Instance.ShowAd();
    }

    public void WatchAdForExtraStarTime()
    {
        //Debug.Log("before deactivating win ui");
        if (uiToDeactivate != null)
        {
            uiToDeactivate.SetActive(false);

        }
        //Debug.Log("win ui deactivated");
        //MaioAd.Instance.ShowAd();
        GoogleMobileAdHolder.Instance.ShowAd();

        gameObject.SetActive(false);
    }

    public void Close()
    {
        winUI.TurnOnButtons();
        gameObject.SetActive(false);
    }
}
