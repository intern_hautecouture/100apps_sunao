﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WinUI : MonoBehaviour
{

    public GameObject[] star;
    public Animation Anim;

    public GameObject ScoreText;
    public GameObject ScoreTargetText;
    public GameObject ScoreHightText;
    private int GameScore;

    public GameObject retryBtn;
    public GameObject homeBtn;
    public GameObject nextBtn;

    public IEnumerator Start()
    {
        //print("start");
        Application.targetFrameRate = 60;
        GameScore = (int)PlayerInfo.Info.Score;
        SetNumberTarget(PlayerInfo.TargetScore);
        SetNumberHiScore((int)PlayerInfo.MapPlayer.HightScore);
        SetNumber(GameScore);
        yield return new WaitForSeconds(1.2f);
        StartCoroutine(StarAnimation(PlayerInfo.MapPlayer.LastStars));
    }

    IEnumerator StarAnimation(int s)
    {
        //print("stars in staranimation " + s);
        yield return new WaitForSeconds(0.6f);
        for (int i = 0; i < s; i++)
        {
            star[i].SetActive(true);
            star[i].GetComponent<Animation>().enabled = true;
            yield return new WaitForSeconds(0.5f);
        }
    }

    void SetNumber(int score)
    {
        //ScoreText.GetComponent<TextMesh>().text = score.ToString();
        ScoreText.GetComponent<Text>().text = score.ToString();
    }

    void SetNumberTarget(int score)
    {
        //ScoreTargetText.GetComponent<TextMesh>().text = score.ToString();
        ScoreTargetText.GetComponent<Text>().text = score.ToString();
    }
    void SetNumberHiScore(int score)
    {
        //ScoreHightText.GetComponent<TextMesh>().text = score.ToString();
        ScoreHightText.GetComponent<Text>().text = score.ToString();
    }


    private void OnEnable()
    {
        //print("enable");
        Application.targetFrameRate = 60;
        GameScore = (int)PlayerInfo.Info.Score;
        SetNumberTarget(PlayerInfo.TargetScore);
        SetNumberHiScore((int)PlayerInfo.MapPlayer.HightScore);
        SetNumber(GameScore);
        //print("last stars before animation: " + PlayerInfo.MapPlayer.LastStars);
        StartCoroutine(StarAnimation(PlayerInfo.MapPlayer.LastStars));
    }

    public void Retry()
    {
        //execute button animation
        retryBtn.GetComponent<Animation>().Play();
        Sound.sound.click();
        StartCoroutine(ButtonActionController.p_retry());
    }

    public void Home()
    {
        homeBtn.GetComponent<Animation>().Play();
        Sound.sound.click();
        StartCoroutine(ButtonActionController.p_menu());
    }

    public void Next()
    {
        nextBtn.GetComponent<Animation>().Play();
        Sound.sound.click();
        StartCoroutine(ButtonActionController.p_next());
    }

    public void TurnOffButtons()
    {
        print("turn off");
        retryBtn.GetComponent<UnityEngine.UI.Button>().interactable = false;
        homeBtn.GetComponent<UnityEngine.UI.Button>().interactable = false;
        nextBtn.GetComponent<UnityEngine.UI.Button>().interactable = false;
    }

    public void TurnOnButtons()
    {
        print("turn on");
        retryBtn.GetComponent<UnityEngine.UI.Button>().interactable = true;
        homeBtn.GetComponent<UnityEngine.UI.Button>().interactable = true;
        nextBtn.GetComponent<UnityEngine.UI.Button>().interactable = true;
    }
}
