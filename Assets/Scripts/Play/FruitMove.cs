﻿using UnityEngine;
using System.Collections;

public class FruitMove : MonoBehaviour
{

    public float pos;
    private float speed = 7.5f;
    private Transform m_Transform;
    private FruitMove me;
    public Fruit f;
    public bool FLAG;

    void Start()
    {
        m_Transform = transform;
        me = GetComponent<FruitMove>();
    }

    void Update()
    {
        MoveToY(pos);
    }
    /// <summary>
    /// Move fruit by y axis
    /// </summary>
    /// <param name="y"></param>
    void MoveToY(float y)
    {

        if (m_Transform.localPosition.y - y >= 0.1f)
        {
            m_Transform.localPosition -= new Vector3(0, Time.smoothDeltaTime * speed, 0);
        }
        else
        {
            m_Transform.localPosition = new Vector3(m_Transform.localPosition.x, y, m_Transform.localPosition.z);
            f.DropEffect();
            if (FLAG)
            {
                GameController.Action.isProcess = false;
                FLAG = false;
            }
            me.enabled = false;
        }

    }
}
