﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GroupMoveControl : MonoBehaviour
{

    public static bool isMove;
    public static bool clickseasion = false;
    public static bool Clicked = true;
    public Transform m_tranform;
    public GameObject Group;
    public GameObject MapObj;
    GroupMap _GroupMap;
    public float leftPos = -5.7f;
    public float rightPos = 5.7f;
    private const float speed = 15f;

    private float MousePos;
    private float delta;

    private GameObject tmp;
    private Vector2 vttmp;
    private Vector2 baseMapPos = new Vector2(-1.92f, 3.14f);
    private Group groupchil;
    private Map chilmap;

    private GameObject tmpGroup;
    private int numberOfGroup = 0;
    private static int indexOfGroup = 0;

    public GameObject ButtonNext;
    public GameObject ButtonPreview;

    private MultiResolution multiRes;

    //public NendAdBanner banner;


    void Start()
    {
        isMove = false;
        Application.targetFrameRate = 60;
        multiRes = GetComponent<MultiResolution>();
        GroupCreate();
        _GroupMap = m_tranform.gameObject.GetComponent<GroupMap>();
    }

    /// <summary>
    /// Draw button
    /// </summary>
    void GroupCreate()
    {
        int numberOfLevel = PlayerPrefs.GetInt(MapManager.NUMBER_OF_LEVEL);

        int currentLevel = 0;

        float currentPosX = 0;

        if (numberOfLevel <= 15)
        {
            numberOfGroup = 1;
        }
        else
        {
            int div = numberOfLevel / 15;
            int mod = numberOfLevel % 15;
            if (mod != 0)
            {
                numberOfGroup = div + 1;
            }
            else
            {
                numberOfGroup = div;
            }
        }

        if (numberOfGroup <= 1)
        {
            ButtonNext.SetActive(false);
            ButtonPreview.SetActive(false);
        }
        else
        {
            ButtonNext.SetActive(true);
            ButtonPreview.SetActive(true);
        }


        for (int i = 0; i < numberOfGroup; i++) // create group
        {
            //Debug.Log("currposx: " + currentPosX);
            tmpGroup = (GameObject)Instantiate(Group, new Vector3(currentPosX, Group.transform.position.y, Group.transform.position.z), Quaternion.identity);
            //Debug.Log("posGroup: " + tmpGroup.transform.position);
            tmpGroup.transform.SetParent(m_tranform);
            tmpGroup.transform.localScale = Vector3.one;
           // m_tranform.localPosition = new Vector2(m_tranform.localPosition.x, -3f);

            for (int j = 0; j < 4; j++)// 5 row
            {
                for (int k = 0; k < 4; k++) // 4 column
                {
                    currentLevel++;
                    if (currentLevel > numberOfLevel) break;

                    vttmp = new Vector2(baseMapPos.x + 1.28f * k, baseMapPos.y - 1.35f * j);
                    tmp = (GameObject)Instantiate(MapObj);
                    tmp.transform.parent = tmpGroup.transform;
                    tmp.transform.localPosition = vttmp;
                    tmp.name = "Item";
                    tmp.GetComponent<Map>().SetPlayer(currentLevel);
                    tmp.GetComponent<Map>().SetMap();
                }
            }
            currentPosX += rightPos * multiRes.GetPercentScale();
        }
        m_tranform.localPosition = new Vector2(m_tranform.localPosition.x, -1.8f);
    }

    void Update()
    {
        // disable and enable button next, preview
        if (numberOfGroup == 1)
        {
            ButtonPreview.SetActive(false);
            ButtonNext.SetActive(false);
        }
        else if (numberOfGroup > 1 && indexOfGroup == 0)
        {
            ButtonPreview.SetActive(false);
            ButtonNext.SetActive(true);
        }
        else if (indexOfGroup >= numberOfGroup - 1)
        {
            ButtonNext.SetActive(false);
            ButtonPreview.SetActive(true);
        }
        else
        {
            ButtonNext.SetActive(true);
            ButtonPreview.SetActive(true);
        }

        TouchProcess();
    }

    void TouchProcess()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 touchPos = new Vector2(wp.x, wp.y);
            if (Physics2D.OverlapPoint(touchPos))
            {
                GameObject ObjPointer = Physics2D.OverlapPoint(touchPos).gameObject;
                if (ObjPointer.gameObject.name == "Back")// when clicked GameObject name back on scene
                {
                    if (!isMove)
                    {
                        indexOfGroup = indexOfGroup - 1;
                        if (indexOfGroup < 0)
                        {
                            indexOfGroup = 0;
                            return;
                        }
                        if (multiRes.GetPercentScale() > 1)
                            _GroupMap.Moveto(leftPos * indexOfGroup * multiRes.GetPercentScale(), speed);
                        else
                            _GroupMap.Moveto(leftPos * indexOfGroup, speed);
                    }
                }
                else if (ObjPointer.gameObject.name == "Next")// when clicked GameObject name back on scene
                {
                    if (!isMove)
                    {
                        indexOfGroup++;
                        if (indexOfGroup >= numberOfGroup)
                        {
                            indexOfGroup = numberOfGroup - 1;
                            return;
                        }
                        if (multiRes.GetPercentScale() > 1)
                            _GroupMap.Moveto(leftPos * indexOfGroup * multiRes.GetPercentScale(), speed);
                        else
                            _GroupMap.Moveto(leftPos * indexOfGroup, speed);
                    }
                }
            }
        }

        // key back on mobile
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ButtonActionController.s_backnoeffect();
        }
    }

}
