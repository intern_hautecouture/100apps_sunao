﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class DataLoader : MonoBehaviour
{
    public LevelData levelData;

    void Start()
    {
        // delete all map data
        //MapManager.DeleteAllMap();

        //initialize map data
        //Debug.Log(System.Environment.Version);
        MapManager.defaultMap(levelData);
        //MapManager.NewLevel();
    }
}
