﻿using UnityEngine;
using System.Collections;

public class GroupMap : MonoBehaviour
{

    private Transform m_tranform;
    private const float dpf = 0.01f;
    private float X;

    void Start()
    {
        m_tranform = transform;
    }

    public void Moveto(float x, float speed)
    {
        X = x;
        GroupMoveControl.clickseasion = true;
        StartCoroutine(_MoveTo(X, speed));
    }

    /// <summary>
    /// Move game object by x axis
    /// </summary>
    /// <param name="x">float</param>
    /// <param name="speed">float</param>
    IEnumerator _MoveTo(float x, float speed)
    {
        GroupMoveControl.isMove = true;
        while (Mathf.Abs(x - m_tranform.localPosition.x) > 0.15)
        {
            if (m_tranform.localPosition.x > x)
            {
                m_tranform.localPosition -= new Vector3(dpf * speed, 0, 0);
            }
            else if (m_tranform.localPosition.x < x)
            {
                m_tranform.localPosition += new Vector3(dpf * speed, 0, 0);
            }

            yield return null;
        }

        m_tranform.localPosition = new Vector3(x, m_tranform.localPosition.y, m_tranform.localPosition.z);
        GroupMoveControl.isMove = false;
    }
}
