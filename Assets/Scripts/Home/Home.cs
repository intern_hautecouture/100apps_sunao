﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Home : MonoBehaviour
{
    public static Home get;
    public Image[] SoundSprite;
    private Sprite sprite;


	public GameObject settingPanel;

    void Start()
    {
        get = this;
        defautButton();
       // nendbanenr.Show();
        //GoogleMobileAdsScript.advertise.HideBanner();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    void defautButton()
    {
        int soundstate = PlayerPrefs.GetInt(ButtonController.SOUNDSTATE);
        int musicstate = PlayerPrefs.GetInt(ButtonController.MUSICSTATE);

        if (soundstate == 0)
        {
            SoundSprite[0].sprite = ButtonController.m_ButtonController.ButtonSprite[0];
            Sound.isSound = true;
        }
        else
        {
            SoundSprite[0].sprite = ButtonController.m_ButtonController.ButtonSprite[1];
            Sound.isSound = false;
        }

        if (musicstate == 0)
        {
            SoundSprite[1].sprite = ButtonController.m_ButtonController.ButtonSprite[2];
            Music.music.MusicON();
        }
        else
        {
            SoundSprite[1].sprite = ButtonController.m_ButtonController.ButtonSprite[3];
            Music.music.MusicOFF();
        }
    }
		

    public void SettingPanelMove()
    {
        settingPanel.SetActive(true);    
    }

    public void CloceSettingPanel()
    {
        settingPanel.SetActive(false);
    }

}
