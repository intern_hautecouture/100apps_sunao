﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour
{

    public static SoundController m_SoundController;

    void Awake()
    {
        if (m_SoundController == null)
        {
            DontDestroyOnLoad(gameObject);
            m_SoundController = this;
        }
        else if (m_SoundController != this)
        {
            Destroy(gameObject);
        }
    }

}
