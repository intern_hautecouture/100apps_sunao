﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour
{

    public static Music music;
    public static bool isMusic;
    public AudioSource audiosource;
    void Awake()
    {
        if (music == null)
        {
            DontDestroyOnLoad(gameObject);
            music = this;
        }
        else if (music != this)
        {
            Destroy(gameObject);
        }
    }

    public void MusicON()
    {
        audiosource.mute = false;
    }
    public void MusicOFF()
    {
        audiosource.mute = true;
    }

}
