﻿using UnityEngine;
using System.Collections;

public class MapManager
{
    public const string LEVEL_ = "Level";
    public const string LOCKED_ = "Locked";
    public const string STARS_ = "Stars";
    public const string HIGH_SCORE_ = "HightScore";
    public const string TARGET_ = "Target";
    public const string GAME_TIME_ = "GameTime";

    public const int NUMBER_OF_LEVEL_DEFAULT = 90;

    public const string NUMBER_OF_LEVEL = "NumberOfLevel";

    /// <summary>
    /// Create map when the first run or no data
    /// </summary>

    public static void NewLevel()
    {
        PlayerPrefs.DeleteAll();
        int target = 1500;
        int time = 60;
        PlayerPrefs.SetInt(NUMBER_OF_LEVEL, 100);

        for (int i = 0; i < 50; i++)
        {
            int level = i + 1;
            PlayerPrefs.SetInt(MapManager.LEVEL_ + level, level);

            if (i == 0)
                PlayerPrefs.SetString(MapManager.LOCKED_ + level, "1");
            else
                PlayerPrefs.SetString(MapManager.LOCKED_ + level, "0");
            PlayerPrefs.SetInt(MapManager.STARS_ + level, 0);
            PlayerPrefs.SetInt(MapManager.HIGH_SCORE_ + level, 0);

            if (level == 1)
                target = 1500;
            else if (level < 10)
                target += 100;
            else if (level == 10)
                target += 200;
            else if (level == 19 || (level >= 26 && level <= 30) || level == 33 || level == 34)
                target += 100;
            else if (level % 20 == 0 || level == 31 || level == 11)
                target += 500;
            else if (level == 39)
                target += 300;
            else
                target += 200;

            if (level % 10 == 1 && level != 1)
                time += 10;

            PlayerPrefs.SetFloat(MapManager.GAME_TIME_ + level, time);

            PlayerPrefs.SetInt(MapManager.TARGET_ + level, target);
        }

        target = 2000;
        time = 50;
        for (int i = 50; i < 75; i++)
        {
            int level = i + 1;
            if (i % 5 == 0)
            {
                time += 10;
            }
            PlayerPrefs.SetInt(MapManager.LEVEL_ + level, level);
            PlayerPrefs.SetString(MapManager.LOCKED_ + level, "0");
            PlayerPrefs.SetInt(MapManager.STARS_ + level, 0);
            PlayerPrefs.SetInt(MapManager.HIGH_SCORE_ + level, 0);
            PlayerPrefs.SetFloat(MapManager.GAME_TIME_ + level, time);
            PlayerPrefs.SetInt(MapManager.TARGET_ + level, target);

            if (i % 5 == 0 && i != 50)
            {
                target += 100;
            }
            else
            {
                target += 75;
            }
        }
        target = 2000;
        time = 50;
        for (int i = 75; i < 100; i++)
        {
            int level = i + 1;
            if (i % 5 == 0)
                time += 10;
            PlayerPrefs.SetInt(MapManager.LEVEL_ + level, level);
            PlayerPrefs.SetString(MapManager.LOCKED_ + level, "0");
            PlayerPrefs.SetInt(MapManager.STARS_ + level, 0);
            PlayerPrefs.SetInt(MapManager.HIGH_SCORE_ + level, 0);
            PlayerPrefs.SetFloat(MapManager.GAME_TIME_ + level, time);
            PlayerPrefs.SetInt(MapManager.TARGET_ + level, target);

            if (i % 5 == 0 && i != 75)
            {
                target += 100;
            }
            else
            {
                target += 50;
            }
        }

        PlayerPrefs.Save();
    }

    public static void defaultMap(LevelData levelData)
    {
        if (PlayerPrefs.HasKey(LEVEL_ + 1))
        {
            Debug.LogError("has key level");
            return;
        }
        PlayerPrefs.DeleteAll();

        Debug.LogError("leveldata number " + levelData.levelNumber);
        int level = 0;
        PlayerPrefs.SetInt(NUMBER_OF_LEVEL, levelData.levelNumber);

        for (int i = 0; i < levelData.levelNumber; i++)
        {
            level = i + 1;

            PlayerPrefs.SetInt(MapManager.LEVEL_ + level, level);
            if (i == 0)
                PlayerPrefs.SetString(MapManager.LOCKED_ + level, "1");
            else
                PlayerPrefs.SetString(MapManager.LOCKED_ + level, "0");
            PlayerPrefs.SetInt(MapManager.STARS_ + level, 0);
            PlayerPrefs.SetInt(MapManager.HIGH_SCORE_ + level, 0);
            PlayerPrefs.SetFloat(MapManager.GAME_TIME_ + level, levelData.levelTimes[i]);
            PlayerPrefs.SetInt(MapManager.TARGET_ + level, levelData.levelTargetScore[i]);
        }
        PlayerPrefs.Save();
        Debug.LogError("lvl quant " + PlayerPrefs.GetInt(NUMBER_OF_LEVEL));
    }

    /// <summary>
    /// Delete all data of PlyerPrefs
    /// </summary>
    public static void DeleteAllMap()
    {
        PlayerPrefs.DeleteAll();
    }

    /// <summary>
    /// Delete data map by level
    /// </summary>
    /// <param name="level">int</param>
    public static void DeleteMapByLevel(int level)
    {
        if (PlayerPrefs.HasKey(LEVEL_ + level))
        {
            PlayerPrefs.DeleteKey(LEVEL_ + level);
            PlayerPrefs.DeleteKey(LOCKED_ + level);
            PlayerPrefs.DeleteKey(STARS_ + level);
            PlayerPrefs.DeleteKey(HIGH_SCORE_ + level);
            PlayerPrefs.DeleteKey(TARGET_ + level);
            PlayerPrefs.DeleteKey(GAME_TIME_ + level);

            PlayerPrefs.SetInt(NUMBER_OF_LEVEL, PlayerPrefs.GetInt(NUMBER_OF_LEVEL) - 1);
            PlayerPrefs.Save();
        }
    }

    /// <summary>
    /// Add level to list map
    /// </summary>
    /// <param name="level">int</param>
    /// <param name="locked">string</param>
    /// <param name="star">int</param>
    /// <param name="hightScore">int</param>
    /// <param name="targetScore">int</param>
    /// <param name="time">float</param>
    public static void AddMap(int level, string locked, int star, int hightScore, int targetScore, float time)
    {
        if (!PlayerPrefs.HasKey(LEVEL_ + level))
        {
            PlayerPrefs.SetInt(LEVEL_ + level, level);
            PlayerPrefs.SetString(LOCKED_ + level, locked);
            PlayerPrefs.SetInt(STARS_ + level, star);
            PlayerPrefs.SetInt(HIGH_SCORE_ + level, hightScore);
            PlayerPrefs.SetInt(TARGET_ + level, targetScore);
            PlayerPrefs.SetFloat(GAME_TIME_ + level, time);

            PlayerPrefs.SetInt(NUMBER_OF_LEVEL, PlayerPrefs.GetInt(NUMBER_OF_LEVEL) + 1);
            PlayerPrefs.Save();
        }
    }

    /// <summary>
    /// Update level of list by id
    /// </summary>
    /// <param name="level">int</param>
    /// <param name="star">int</param>
    /// <param name="hightScore">int</param>
    public static void UpdateMapByLevel(int level, int star, int hightScore)
    {
        if (PlayerPrefs.HasKey(LEVEL_ + level))
        {
            PlayerPrefs.SetInt(STARS_ + level, star);
            PlayerPrefs.SetInt(HIGH_SCORE_ + level, hightScore);
            int nextlevel = level + 1;
            if (nextlevel <= PlayerPrefs.GetInt(MapManager.NUMBER_OF_LEVEL))
            {
                // unlock the next level
                PlayerPrefs.SetString(LOCKED_ + nextlevel, "1");
            }
            PlayerPrefs.Save();
        }
    }

    public static void UpdateMapByLevel(int level, string locked, int star, int hightScore, int targetScore, float time)
    {
        if (PlayerPrefs.HasKey(LEVEL_ + level))
        {
            PlayerPrefs.SetString(LOCKED_ + level, locked);
            PlayerPrefs.SetInt(STARS_ + level, star);
            PlayerPrefs.SetInt(HIGH_SCORE_ + level, hightScore);
            PlayerPrefs.SetInt(TARGET_ + level, targetScore);
            PlayerPrefs.SetFloat(GAME_TIME_ + level, time);
            PlayerPrefs.Save();
        }
    }
}