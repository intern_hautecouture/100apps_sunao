﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class ButtonActionController : MonoBehaviour
{

    public static IEnumerator h_playbutton()
    {
        ButtonController.IsTouch = true;
        yield return new WaitForSeconds(0.5f);
        ButtonController.IsTouch = false;
        SceneManager.LoadScene("MapsScene");
    }

    public static void h_soundbutton()
    {
        int soundstate = PlayerPrefs.GetInt(ButtonController.SOUNDSTATE, 0);
        if (soundstate == 0)
        {
            PlayerPrefs.SetInt(ButtonController.SOUNDSTATE, 1);
            Sound.isSound = false;
        }
        else
        {
            PlayerPrefs.SetInt(ButtonController.SOUNDSTATE, 0);
            Sound.isSound = true;
        }
    }

    public static void h_musicbutton()
    {
        int musicstate = PlayerPrefs.GetInt(ButtonController.MUSICSTATE, 0);
        if (musicstate == 0)
        {
            PlayerPrefs.SetInt(ButtonController.MUSICSTATE, 1);
            Music.music.MusicOFF();
        }
        else
        {
            PlayerPrefs.SetInt(ButtonController.MUSICSTATE, 0);
            Music.music.MusicON();

        }
    }
    public static IEnumerator s_back()
    {
        ButtonController.IsTouch = true;
        yield return new WaitForSeconds(0.5f);
        ButtonController.IsTouch = false;
        SceneManager.LoadScene("HomeScene");
    }

    public static void s_backnoeffect()
    {
        SceneManager.LoadScene("HomeScene");
    }
    public static void s_Item(Player player)
    {
        PlayerInfo.MapPlayer = player;
        PlayerInfo.Info.Score = 0;
        SceneManager.LoadScene("PlayScene");
    }
    public static void p_pause()
    {
        Timer.tick = false;
        Timer.GameState = 1;
        Timer.get.Pause();
    }

    

    public static IEnumerator p_resume()
    {
        ButtonController.IsTouch = true;
        yield return new WaitForSeconds(0.5f);
        Timer.get.Resume();
        ButtonController.IsTouch = false;
    }
    public static IEnumerator p_retry()
    {
        ButtonController.IsTouch = true;
        yield return new WaitForSeconds(0.5f);
        PlayerInfo.Info.Score = 0;
        ButtonController.IsTouch = false;
        SceneManager.LoadScene("PlayScene");
    }
    public static IEnumerator p_menu()
    {
        ButtonController.IsTouch = true;
        Timer.get.Menu();
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("MapsScene");
        ButtonController.IsTouch = false;
    }
    public static IEnumerator p_next()
    {
        ButtonController.IsTouch = true;
        yield return new WaitForSeconds(0.5f);
        Player p;
        int nextLevel = PlayerInfo.MapPlayer.Level + 1;

        if (nextLevel <= PlayerPrefs.GetInt(MapManager.NUMBER_OF_LEVEL))
        {
            p = new Player();
            p.Level = PlayerPrefs.GetInt(MapManager.LEVEL_ + nextLevel);
            p.Locked = PlayerPrefs.GetString(MapManager.LOCKED_ + nextLevel) == "0" ? true : false;
            p.Stars = PlayerPrefs.GetInt(MapManager.STARS_ + nextLevel);
            p.Target = PlayerPrefs.GetInt(MapManager.TARGET_ + nextLevel);
            p.HightScore = PlayerPrefs.GetInt(MapManager.HIGH_SCORE_ + nextLevel);
            p.GameTime = PlayerPrefs.GetFloat(MapManager.GAME_TIME_ + nextLevel);

            PlayerInfo.MapPlayer = p;
            PlayerInfo.Info.Score = 0;
            SceneManager.LoadScene("PlayScene");
        }
        ButtonController.IsTouch = false;
    }

    public static IEnumerator p_setting()
    {
        yield return new WaitForSeconds(0.5f);
        Home.get.SettingPanelMove();
    }

    public static IEnumerator p_cloce()
    {
        yield return new WaitForSeconds(0.5f);
        Home.get.CloceSettingPanel();
    }
}
