﻿using UnityEngine;
using System.Collections;

public class DropAll : MonoBehaviour {

    public float timedrop =10f;
    bool isdrop = false;
    void Start()
    {
        timedrop = 10f;
      
    }
	void Update () {
        if (timedrop > 0)
            {
                isdrop = false;
               timedrop -= Time.deltaTime;
              }
        else
        {
            if (!isdrop)
            {
                isdrop = true;
                StartCoroutine(drop());
            }
        }
	}
    IEnumerator drop()
    {
        EffectSpawner.Spawn.SetTextScore();
        yield return new WaitForSeconds(0.2f);
        GameController.Action.DropAllFruit();
        GameController.Action.Drop.enabled = false;
    }
}
