﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{

    public static ButtonController m_ButtonController;
    public static bool IsTouch;

    // constants
    private const string PLAY = "play_button";
    private const string MUSIC = "music";
    private const string SOUND = "sound";
    private const string BACK = "back";
    private const string PAUSE = "pause";
    private const string RESUME = "resume";
    private const string RETRY = "retry";
    private const string MENU = "menu";
    private const string NEXT = "next";
	private const string SETTING = "setting_button";
    private const string CLOCE = "close_button";
    private const string MUSIC_PLAY = "music_button";
    private const string SOUND_PLAY = "sound_button";
    public const string SOUNDSTATE = "soundstate";
    public const string MUSICSTATE = "musicstate";
    public const string SHARE = "share";
    public const string SHAREFB = "shareFacebook";
    public const string SHARETWITTER = "shareTwitter";
    public const string SHARELINE = "shareLine";
    //for debugging only - delete later
    public const string AD = "ads";
    public const string GO = "gameover";

    public Sprite[] ButtonSprite;

    GameObject Pointer;
    string buttonName;

    Share share;

    void Start()
    {
        share = GetComponent<Share>();
    }

    void Awake()
    {
        if (m_ButtonController == null)
        {
            DontDestroyOnLoad(gameObject);
            m_ButtonController = this;
        }
        else if (m_ButtonController != this)
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        TouchProcess();
    }

    void TouchProcess()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Pointer = TouchChecker(Input.mousePosition);
            if (Pointer != null)
            {
                buttonName = Pointer.name;
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            Pointer = TouchChecker(Input.mousePosition);
            if (Pointer != null && buttonName == Pointer.name)
            {
                ButtonAction(Pointer);
            }
            buttonName = null;
        }
    }

    /// <summary>
    /// Get GameObject when touch on screen
    /// </summary>
    /// <param name="mouseposition">Vector3</param>
    /// <returns>GameObject</returns>
    GameObject TouchChecker(Vector3 mouseposition)
    {
        Vector3 wp = Camera.main.ScreenToWorldPoint(mouseposition);
        Vector2 touchPos = new Vector2(wp.x, wp.y);
        if (Physics2D.OverlapPoint(touchPos))
        {
            return Physics2D.OverlapPoint(touchPos).gameObject;
        }
        return null;
    }
    /// <summary>
    /// Check action and play animation for buttons by name  
    /// </summary>
    /// <param name="button">GameObject</param>
	public void ButtonAction(GameObject button)
    {
        Debug.Log(button.name);

        if (!IsTouch)
        {
            switch (button.name)
            { 
                case PLAY:
                    Sound.sound.click();
                    StartCoroutine(ButtonActionController.h_playbutton());
                    break;
                case SOUND:
                    soundSpriteChange();
                    Sound.sound.click();
                    ButtonActionController.h_soundbutton();
                    break;
                case MUSIC:
                    musicSpriteChange();
                    Sound.sound.click();
                    ButtonActionController.h_musicbutton();
                    break;

                case SOUND_PLAY:
                    soundSpriteChangePlay();
                    Sound.sound.click();
                    ButtonActionController.h_soundbutton();
                    break;
                case MUSIC_PLAY:
                    musicSpriteChangePlay();
                    Sound.sound.click();
                    ButtonActionController.h_musicbutton();
                    break;
                case BACK:
                    Sound.sound.click();
                    StartCoroutine(ButtonActionController.s_back());
                    break;
                case RESUME:
                    Sound.sound.UnPause();
                    Sound.sound.click();
                    StartCoroutine(ButtonActionController.p_resume());
                    break;
                case RETRY:
                    Sound.sound.Stop();
                    Sound.sound.click();
                    StartCoroutine(ButtonActionController.p_retry());
                    break;
                case MENU:
                    Sound.sound.click();
                    StartCoroutine(ButtonActionController.p_menu());
                    break;
                case NEXT:
                    Sound.sound.click();
                    StartCoroutine(ButtonActionController.p_next());
                    break;
                case SETTING:
                    Sound.sound.click();
                    StartCoroutine(ButtonActionController.p_setting());
                    break;
                case CLOCE:
                    Sound.sound.click();
                    StartCoroutine(ButtonActionController.p_cloce());
                    return;
                case SHARE:
                    Debug.Log("SHARE");
                    showShareButtons();
                    break;
                case SHAREFB:
                    share.ShareFacebook();
                    break;
                case SHARETWITTER:
                    share.ShareTwitter();
                    break;
                case SHARELINE:
                    share.ShareLine();
                    break;
                case AD:
                    //MaioAd.Instance.ShowAd();
                    button.SetActive(false);
                    break;
                case GO:
                    Timer.time = 0.001f;
                    break;
                case PAUSE:
                    Sound.sound.Pause();
                    GameObject.Find("Timer").GetComponent<Timer>().Pause();
                    break;
            }
        }

    }

    void soundSpriteChange()
    {

        int soundstate = PlayerPrefs.GetInt(SOUNDSTATE, 0);
        if (soundstate == 0)
			GameObject.Find(SOUND).GetComponent<Image>().sprite = ButtonSprite[1];
        else
			GameObject.Find(SOUND).GetComponent<Image>().sprite = ButtonSprite[0];
    }

    void soundSpriteChangePlay()
    {

        int soundstate = PlayerPrefs.GetInt(SOUNDSTATE, 0);
        if (soundstate == 0)
            GameObject.Find(SOUND_PLAY).GetComponent<Image>().sprite = ButtonSprite[5];
        else
			GameObject.Find(SOUND_PLAY).GetComponent<Image>().sprite = ButtonSprite[4];
    }

    void musicSpriteChange()
    {
        int musicstate = PlayerPrefs.GetInt(MUSICSTATE, 0);
        if (musicstate == 0)
			GameObject.Find(MUSIC).GetComponent<Image>().sprite = ButtonSprite[3];
        else
			GameObject.Find(MUSIC).GetComponent<Image>().sprite = ButtonSprite[2];
    }

    void musicSpriteChangePlay()
    {
        int musicstate = PlayerPrefs.GetInt(MUSICSTATE, 0);
        if (musicstate == 0)
			GameObject.Find(MUSIC_PLAY).GetComponent<Image>().sprite = ButtonSprite[7];
        else
			GameObject.Find(MUSIC_PLAY).GetComponent<Image>().sprite = ButtonSprite[6];
    }

    bool buttonsOpened = false;

    void showShareButtons()
    {
        GameObject shareButtons = GameObject.Find("share");
        //var shareLine = GameObject.Find("shareLine");
        //var shareTwitter = GameObject.Find("shareTwitter");
        //var shareFacebook = GameObject.Find("shareFacebook");
        float transitionTime = 0.8f;

        CancelInvoke();

        var originalPos = new Vector3(0,0,1);
        var posLine = new Vector3(-1.411f, -0.924f, 0);
        var posFacebook = new Vector3(0, -0.924f, 0);
        var posTwitter = new Vector3(1.411f, -0.924f, 0);

        foreach (Transform button in shareButtons.transform)
        {
            var transitionVec = originalPos;

            if (!buttonsOpened)
            {
                button.gameObject.SetActive(true);

                switch (button.name)
                {
                    case "shareFacebook":
                        transitionVec = posFacebook;
                        break;
                    case "shareTwitter":
                        transitionVec = posTwitter;
                        break;
                    case "shareLine":
                        transitionVec = posLine;
                        break;
                }
            }

            iTween.MoveTo(button.gameObject, iTween.Hash(
                "position", transitionVec,
                "isLocal", true,
                "time", transitionTime,
                "easyType", iTween.EaseType.linear
            ));
        }

        if (buttonsOpened) Invoke("HideButtons", 1f);

        buttonsOpened = !buttonsOpened;
    }

    public void HideButtons()
    {
        GameObject shareButtons = GameObject.Find("share");

        foreach (Transform button in shareButtons.transform)
        {
            button.gameObject.SetActive(false);
        }
    }
}
