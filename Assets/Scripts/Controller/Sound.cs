﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sound : MonoBehaviour
{

    // Use this for initialization
    public static Sound sound;
    public static bool isSound;
    public AudioClip[] aclip;

    private float clipVolume = 5;
    private AudioSource audiosource;

    void Awake()
    {
        if (sound == null)
        {
            DontDestroyOnLoad(gameObject);
            sound = this;
            audiosource = GetComponent<AudioSource>();
        }
        else if (sound != this)
        {
            Destroy(gameObject);
        }
    }

    public void Pause()
    {
        audiosource.Pause();
    }

    public void UnPause()
    {
        audiosource.UnPause();
    }

    public void Stop()
    {
        audiosource.Stop();
    }

    public void click()
    {
        if (isSound)
        {
            audiosource.PlayOneShot(aclip[0], clipVolume + 2);
        }
    }
    public void select(int count)
    {
        int idx = count;
        if (idx >9) idx =9;

        if (isSound)
        {
            audiosource.PlayOneShot(aclip[idx], clipVolume);
        }
    }
    public void fire()
    {
        if (isSound)
        {
            audiosource.PlayOneShot(aclip[10], clipVolume);
        }
    }
    public void waring()
    {
        if (isSound)
        {
            audiosource.PlayOneShot(aclip[11], clipVolume);
        }
    }
    public void win()
    {
        if (isSound)
        {
            audiosource.PlayOneShot(aclip[12], clipVolume);
        }
    }
    public void lose()
    {
        if (isSound)
        {
            audiosource.PlayOneShot(aclip[13], clipVolume);
        }
    }
    public void popstar()
    {
        if (isSound)
        {
            audiosource.PlayOneShot(aclip[14], clipVolume);
        }
    }
    public void startgame()
    {
        if (isSound)
        {
            audiosource.PlayOneShot(aclip[15]);
        }
    }
    public void blast()
    {
        if (isSound)
        {
            audiosource.PlayOneShot(aclip[16], clipVolume);
        }
    }
    public void AddPower()
    {
        if (isSound)
        {
            audiosource.PlayOneShot(aclip[17], clipVolume);
        }
    }
}
