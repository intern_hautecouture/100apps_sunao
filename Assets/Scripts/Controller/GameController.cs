﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{

    public static GameController Action;

    public List<GameObject> FruitCollectList;
    private List<GameObject> FruitPowerCollectList;

    public GameObject Screen;
    public GameObject LinePrefab;
    private List<GameObject> ListLines;

    public GameObject Pointer;
    public GameObject LastPointer;
    private GameObject ObjTemp;

    private const string FRUIT_TAG = "fruit";
    private const string FRUIT_SELECT_TAG = "fruitSelected";
    private const float DESTROY_WAIT_TIME = 0.07f;

    private int typeCollect = -1;
    private int tmptype;

    private Fruit FruitScript;
    private Fruit FruitScript1;

    private int CollectCount;
    public bool isProcess;

    private int ScorePerFruit = 0;
    private int type8count = 0;

    public DropAll Drop;
    private GameObject ds = null;

    void Start()
    {
        //banner.Show();

        Application.targetFrameRate = 60;
        Action = this;
        typeCollect = -1;
        ListLines = new List<GameObject>();
        //ImobileAd.Instance.LoadNativeAd();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !isProcess)
        {
            typeCollect = -1;
            type8count = 0;
            FruitCollectList = new List<GameObject>();
            Pointer = FruitTouchChecker(Input.mousePosition);
            if (Pointer != null && Pointer.tag == FRUIT_TAG)
            {
                FruitCollect(Pointer);
                ScorePerFruit = 0;
            }
        }
        else if (Input.GetMouseButtonUp(0) && !isProcess)
        {
            //process here
            CollectCount = FruitCollectList.Count;
            FruitCollectProcess(CollectCount);
            //delete line
            LinesDestroy();

        }
        else if (Input.GetMouseButton(0) && !isProcess)
        {
            Pointer = FruitTouchChecker(Input.mousePosition);

            if (Pointer != null && Pointer.tag == FRUIT_TAG)
            {
                FruitCollect(Pointer);
            }
            else if (Pointer != null && Pointer.tag == FRUIT_SELECT_TAG)
            {

                if (ds != Pointer)
                {
                    DeSelectFruit(Pointer);
                    ds = Pointer;
                }

            }
        }
    }


    /// <summary>
    /// Select fruit same color
    /// </summary>
    /// <param name="Pointer">GameObject</param>
    void FruitCollect(GameObject Pointer)
    {

        FruitScript = Pointer.GetComponent<Fruit>();
        if (typeCollect == -1 && FruitCollectList.Count == 0)
        {
            AddFruitToList(FruitScript, Pointer);
        }
        else if (typeCollect == -1)
        {
            FruitScript1 = FruitCollectList[FruitCollectList.Count - 1].GetComponent<Fruit>();
            if (distanchecker(FruitScript.FruitPosition, FruitScript1.FruitPosition))
            {
                AddFruitToList(FruitScript, Pointer);

                LineSpawner(Pointer, FruitCollectList[FruitCollectList.Count - 2]);
            }
        }
        else if (FruitCollectList.Count > 0)
        {
            FruitScript1 = FruitCollectList[FruitCollectList.Count - 1].GetComponent<Fruit>();
            if (distanchecker(FruitScript.FruitPosition, FruitScript1.FruitPosition) && FruitTypeChecker(FruitScript))
            {
                AddFruitToList(FruitScript, Pointer);

                LineSpawner(Pointer, FruitCollectList[FruitCollectList.Count - 2]);
            }
        }
    }

    /// <summary>
    /// Check type of fruit
    /// </summary>
    /// <param name="FruitScript">Fruit</param>
    /// <returns>bool</returns>
    bool FruitTypeChecker(Fruit FruitScript)
    {
        if (FruitScript.FruitType == typeCollect || FruitScript.FruitType == 8)
        {
            return true;
        }
        return false;
    }

    bool distanchecker(Vector2 a, Vector2 b)
    {
        if (Vector2.Distance(a, b) < 2)
        {
            return true;
        }
        return false;
    }
    GameObject FruitTouchChecker(Vector3 mouseposition)
    {
        Vector3 wp = Camera.main.ScreenToWorldPoint(mouseposition);
        Vector2 touchPos = new Vector2(wp.x, wp.y);
        if (Physics2D.OverlapPoint(touchPos))
        {
            return Physics2D.OverlapPoint(touchPos).gameObject;
        }
        return null;
    }

    void LinesDestroy()
    {
        for (int i = 0; i < ListLines.Count; i++)
        {
            if (ListLines[i] != null)
            {
                Destroy(ListLines[i]);
            }
        }
        ListLines.Clear();
    }

    void LineSpawner(GameObject obj1, GameObject obj2)
    {
        FruitScript = obj1.GetComponent<Fruit>();
        FruitScript1 = obj2.GetComponent<Fruit>();
        ObjTemp = (GameObject)Instantiate(LinePrefab);
        ObjTemp.transform.parent = Screen.transform;
        SetLineRotation(ObjTemp, FruitScript.FruitPosition, FruitScript1.FruitPosition);
        ObjTemp.transform.position = (obj1.transform.position + obj2.transform.position) / 2;
        ListLines.Add(ObjTemp);

    }
    void SetLineRotation(GameObject line, Vector2 pos1, Vector2 pos2)
    {
        if (pos1.x == pos2.x)
        {
            line.transform.localScale = new Vector3(1, 1, 1);
            line.transform.localRotation = Quaternion.Euler(0, 0, 90);
        }
        else if (pos1.y == pos2.y)
        {
            line.transform.localScale = new Vector3(1, 1, 1);
            line.transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        else if ((pos1.x - pos2.x) * (pos1.y - pos2.y) > 0)
        {
            line.transform.localScale = new Vector3(1.5f, 1, 1);
            line.transform.localRotation = Quaternion.Euler(0, 0, 45);
        }
        else
        {
            line.transform.localScale = new Vector3(1.5f, 1, 1);
            line.transform.localRotation = Quaternion.Euler(0, 0, -45);
        }

    }

    void AddFruitToList(Fruit FruitScript, GameObject Pointer)
    {
        if (FruitScript.FruitType != 8)
            typeCollect = FruitScript.FruitType;
        FruitCollectList.Add(Pointer);
        FruitScript.SetTag(FRUIT_SELECT_TAG);
        Sound.sound.select(FruitCollectList.Count);

    }

    void DeSelectFruit(GameObject Pointer)
    {
        if (Pointer != LastPointer)
        {

            if (FruitCollectList.Count > 1 && Pointer == FruitCollectList[FruitCollectList.Count - 2])
            {
                Destroy(ListLines[ListLines.Count - 1]);
                ListLines.RemoveAt(ListLines.Count - 1);
                FruitCollectList[FruitCollectList.Count - 1].GetComponent<Fruit>().SetTag(FRUIT_TAG);
                FruitCollectList.RemoveAt(FruitCollectList.Count - 1);

                if (FruitCollectList.Count == 1 && FruitCollectList[0].GetComponent<Fruit>().FruitType == 8)
                {
                    typeCollect = -1;
                }
                Sound.sound.select(FruitCollectList.Count);
            }
            LastPointer = Pointer;
        }
    }

    void FruitCollectProcess(int count)
    {
        if (count == 1)
        {
            FruitCollectList[0].GetComponent<Fruit>().SetTag(FRUIT_TAG);
        }
        else if (count == 2)
        {
            FruitScript = FruitCollectList[0].GetComponent<Fruit>();
            FruitScript1 = FruitCollectList[1].GetComponent<Fruit>();
            if (FruitScript.FruitPower == 0 && FruitScript1.FruitPower == 0)
            {
                ScorePerFruit = -5;
                FruitProcess(false, 0);
            }
            else
            {
                FruitScript.SetTag(FRUIT_TAG);
                FruitScript1.SetTag(FRUIT_TAG);
            }
        }
        else if (count >= 3 && count < 6)
        {
            ScorePerFruit = (count - 2) * 10;
            FruitProcess(false, 0);

        }
        else if (count == 6 || count == 7)
        {
            ScorePerFruit = (count - 2) * 10;
            FruitProcess(true, Random.Range(1, 3));
        }
        else if (count == 8)
        {
            ScorePerFruit = (count - 2) * 10;
            FruitProcess(true, 3);
        }
        else if (count > 8)
        {
            ScorePerFruit = (count - 2) * 10;
            FruitProcess(true, 4);
        }
    }

    void FruitProcess(bool power, int powertype)
    {
        for (int i = 0; i < FruitCollectList.Count; i++)
        {
            FruitScript = FruitCollectList[i].GetComponent<Fruit>();
            FruitSpawner.Spawner.FruitGrid[(int)FruitScript.FruitPosition.x, (int)FruitScript.FruitPosition.y] = null;
        }

        StartCoroutine(NomalFruitProcess(power, powertype));
    }

    IEnumerator NomalFruitProcess(bool power, int powertype)
    {
        isProcess = true;
        Drop.timedrop = 1f;
        Drop.enabled = true;

        Sound.sound.blast();
        if (power)
            FruitCollectList[0].GetComponent<Fruit>().isPowerUp = true;

        for (int i = 0; i < FruitCollectList.Count; i++)
        {
            DestroyProcess(FruitCollectList[i]);
            yield return new WaitForSeconds(DESTROY_WAIT_TIME);
        }

        if (power)
            AddPower(FruitCollectList[0], powertype);
        FruitCollectList.Clear();
    }
    void AddPower(GameObject obj, int power)
    {
        Sound.sound.AddPower();
        FruitScript = obj.GetComponent<Fruit>();
        FruitSpawner.Spawner.SpawnFruitPower(obj, power);

    }
    bool DestroyProcess(GameObject obj)
    {

        bool b = true;
        FruitScript = obj.GetComponent<Fruit>();
        if (FruitScript.FruitPower > 0)
            b = false;
        if (FruitScript.FruitType == 8)
            type8count++;
        if (FruitScript.FruitType == 8 && type8count > 1)
        {
            FruitScript.DropEffect();
            FruitSpawner.Spawner.FruitGrid[(int)FruitScript.FruitPosition.x, (int)FruitScript.FruitPosition.y] = obj;
            return b;
        }
        EffectSpawner.Spawn.ScoreSpawn(obj, ScorePerFruit);
        FruitScript.FruitDestroy(true);
        return b;
    }

    public void FruitPowerProcess(int power, Vector2 fruitposition, Vector3 position)
    {
        switch (power)
        {
            case 1:
                Sound.sound.fire();
                RowDes(fruitposition);
                break;
            case 2:
                Sound.sound.fire();
                CollDes(fruitposition);
                break;
            case 3:
                Sound.sound.fire();
                RowDes(fruitposition);
                CollDes(fruitposition);
                break;
            case 4:
                Sound.sound.fire();
                DestroyAllType(typeCollect);
                break;
        }
        EffectSpawner.Spawn.DestroyEffectPower(Screen, power, position);

    }

    void RowDes(Vector2 fruitposition)
    {
        RowDesLeft(fruitposition);
        RowDesRight(fruitposition);
    }

    void CollDes(Vector2 fruitposition)
    {
        RowDesUp(fruitposition);
        RowDesDown(fruitposition);
    }

    void RowDesLeft(Vector2 fruitposition)
    {
        List<GameObject> tmplist = new List<GameObject>();

        for (int i = (int)fruitposition.x - 1; i >= 0; i--)
        {
            ObjTemp = FruitSpawner.Spawner.FruitGrid[i, (int)fruitposition.y];
            if (ObjTemp != null && ObjTemp.tag != FRUIT_SELECT_TAG)
            {
                tmplist.Add(ObjTemp);
                FruitSpawner.Spawner.FruitGrid[i, (int)fruitposition.y] = null;
            }
        }
        StartCoroutine(DestroyList(tmplist));

    }
    void RowDesRight(Vector2 fruitposition)
    {
        List<GameObject> tmplist = new List<GameObject>();

        for (int i = (int)fruitposition.x + 1; i < 7; i++)
        {
            ObjTemp = FruitSpawner.Spawner.FruitGrid[i, (int)fruitposition.y];
            if (ObjTemp != null && ObjTemp.tag != FRUIT_SELECT_TAG)
            {
                tmplist.Add(ObjTemp);
                FruitSpawner.Spawner.FruitGrid[i, (int)fruitposition.y] = null;
            }
        }
        StartCoroutine(DestroyList(tmplist));

    }

    void RowDesUp(Vector2 fruitposition)
    {
        List<GameObject> tmplist = new List<GameObject>();

        for (int i = (int)fruitposition.y - 1; i >= 0; i--)
        {
            ObjTemp = FruitSpawner.Spawner.FruitGrid[(int)fruitposition.x, i];
            if (ObjTemp != null && ObjTemp.tag != FRUIT_SELECT_TAG)
            {
                tmplist.Add(ObjTemp);
                FruitSpawner.Spawner.FruitGrid[(int)fruitposition.x, i] = null;
            }
        }
        StartCoroutine(DestroyList(tmplist));

    }
    void RowDesDown(Vector2 fruitposition)
    {
        List<GameObject> tmplist = new List<GameObject>();

        for (int i = (int)fruitposition.y + 1; i < 7; i++)
        {
            ObjTemp = FruitSpawner.Spawner.FruitGrid[(int)fruitposition.x, i];
            if (ObjTemp != null && ObjTemp.tag != FRUIT_SELECT_TAG)
            {
                tmplist.Add(ObjTemp);
                FruitSpawner.Spawner.FruitGrid[(int)fruitposition.x, i] = null;
            }
        }
        StartCoroutine(DestroyList(tmplist));

    }

    IEnumerator DestroyList(List<GameObject> tmplist)
    {
        for (int i = 0; i < tmplist.Count; i++)
        {
            if (tmplist != null)
            {
                FruitScript = tmplist[i].GetComponent<Fruit>();
                EffectSpawner.Spawn.ScoreSpawn(tmplist[i], ScorePerFruit);
                FruitScript.FruitDestroy(false);
                yield return new WaitForSeconds(DESTROY_WAIT_TIME);
            }
        }
    }

    /// <summary>
    /// Drop all the fruit on map
    /// </summary>
    public void DropAllFruit()
    {

        for (int y = 0; y < 7; y++)
            for (int x = 0; x < 7; x++)
            {
                if (FruitSpawner.Spawner.FruitGrid[x, y] != null)
                {
                    FruitScript = FruitSpawner.Spawner.FruitGrid[x, y].GetComponent<Fruit>();
                    FruitScript.Drop();
                }
            }
        FruitSpawner.Spawner.AddFruit();
    }

    void SetFruitNewPosition()
    {
        for (int y = 0; y < 7; y++)
            for (int x = 0; x < 7; x++)
            {
                if (FruitSpawner.Spawner.FruitGrid[x, y] != null)
                {
                    FruitScript = FruitSpawner.Spawner.FruitGrid[x, y].GetComponent<Fruit>();
                    FruitScript.SetNewPosition();
                }
            }
    }

    void DestroyAllType(int type)
    {
        if (type != 8)
        {
            List<GameObject> tmpList = getAllType(type);

            StartCoroutine(DestroyList(tmpList));
        }

    }

    List<GameObject> getAllType(int type)
    {
        List<GameObject> tmpList = new List<GameObject>();

        for (int x = 0; x < 7; x++)
        {
            for (int y = 0; y < 7; y++)
            {
                if (FruitSpawner.Spawner.FruitGrid[x, y] != null && FruitSpawner.Spawner.FruitGrid[x, y].tag != FRUIT_SELECT_TAG && FruitSpawner.Spawner.FruitGrid[x, y].GetComponent<Fruit>().FruitType == type)
                {
                    tmpList.Add(FruitSpawner.Spawner.FruitGrid[x, y]);
                    FruitSpawner.Spawner.FruitGrid[x, y] = null;
                }
            }
        }

        return tmpList;
    }
}
