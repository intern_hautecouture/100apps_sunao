﻿using UnityEngine;
using System.Collections;

public class loadingwait : MonoBehaviour
{

    void Start()
    {
        StartCoroutine(LoadLevelByFade());
    }

    /// <summary>
    /// auto tranfer Home Scene after 3.6 second.
    /// </summary>
    IEnumerator LoadLevelByFade()
    {
        yield return new WaitForSeconds(3.6f);
        float fadeTime = GameObject.Find("Screen").GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime);

        Application.LoadLevel("HomeScene");
    }

}
