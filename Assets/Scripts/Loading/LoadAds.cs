﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadAds : MonoBehaviour
{

    public static LoadAds load;     //instance of LoadAds 
    //public GoogleAnalyticsV3 GA;    // instance google analytic
    void Awake()
    {
        if (load == null)
        {
            DontDestroyOnLoad(gameObject);
            load = this;
        }
        else if (load != this)
        {
            Destroy(gameObject);
        }
        Application.targetFrameRate = 60;

    }
    void Start()
    {
        // start session of google analytic
        //GA.StartSession();
    }
}
