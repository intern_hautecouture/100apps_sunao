﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Player
{
    // information player by level
    public int Level;
    public bool Locked;
    public int Stars;
    public int LastStars;
    public int HightScore;
    public int Target;
    public float GameTime;
}
