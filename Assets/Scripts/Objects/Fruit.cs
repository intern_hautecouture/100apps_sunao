﻿using UnityEngine;
using System.Collections;

public class Fruit : MonoBehaviour
{

    public Vector2 FruitPosition;
    public int FruitType;
    public int FruitPower;

    public bool isPowerUp;

    public Animation SelectAnim;
    public Animation RenderAnimation;
    public SpriteRenderer RenderSprite;
    public SpriteRenderer SelectSprite;

    public FruitMove Move;

    private const string FRUIT_TAG = "fruit";
    private const string FRUIT_SELECT_TAG = "fruitSelected";

    private int newpos;
    private Vector2 vector2tmp;

    private const float DROP_WAIT_TIME = 0.2f;

    void Start()
    {
        SelectAnim = transform.Find("Selected").GetComponent<Animation>();
    }

    /// <summary>
    /// Set tag fruit is selected
    /// </summary>
    /// <param name="tag"></param>
    public void SetTag(string tag)
    {
        this.tag = tag;
        if (tag == FRUIT_TAG)
        {
            DeSelect();
        }
        else if (tag == FRUIT_SELECT_TAG)
        {
            SelectedEffect();
        }
    }
    public void DropEffect()
    {
        DeSelect();
        RenderAnimation.enabled = true;
        RenderAnimation.Play(FRUIT_TAG);
        this.tag = FRUIT_TAG;
    }

    void SelectedEffect()
    {
        SelectAnim.gameObject.SetActive(true);
        SelectAnim.enabled = true;
        SelectAnim.Play("FruitSelected");
    }
    void DeSelect()
    {
        SelectAnim.gameObject.SetActive(false);
        SelectAnim.enabled = false;
        SelectAnim.Stop("FruitSelected");
    }

    public void SetSpritePower4()
    {
        //Debug.Log("power4");
        //Debug.Log("fruitpower " + FruitPower);
        RenderSprite.sprite = FruitSpawner.Spawner.FruitSprite[8];
        SelectSprite.sprite = FruitSpawner.Spawner.FruitSpriteHold[8];
    }

    public void FruitDestroy(bool b)
    {
        GameController.Action.Drop.timedrop = DROP_WAIT_TIME;

        if (FruitPower == 4)
            EffectSpawner.Spawn.HeartVFX(gameObject);
        else
        {
            if (b)
                EffectSpawner.Spawn.Blast(gameObject);
            else
                EffectSpawner.Spawn.BlastPark(gameObject);
        }

        if (FruitPower > 0)
        {
            GameController.Action.FruitPowerProcess(FruitPower, FruitPosition, transform.position);
            for (int i = 2; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }

        transform.GetChild(1).gameObject.SetActive(false);
        FruitSpawner.Spawner.FruitGrid[(int)FruitPosition.x, (int)FruitPosition.y] = null;
        gameObject.transform.localPosition = new Vector3(FruitSpawner.rootPos.x + FruitPosition.x * FruitSpawner.CollumnSpace,
                                                         FruitSpawner.Spawner.RespawnFruitList[(int)FruitPosition.x].Count * FruitSpawner.RowSpace + FruitSpawner.PosY);

        //add obj to list waiting
        if (!isPowerUp)
            FruitSpawner.Spawner.RespawnFruitList[(int)FruitPosition.x].Add(gameObject);
    }

    /// <summary>
    /// Find new fruit position. If new position not equal current position. Set new postion and move to new position
    /// </summary>
    public void Drop()
    {
        vector2tmp = FruitPositionFinder(FruitPosition);
        if (vector2tmp != FruitPosition)
        {
            FruitSpawner.Spawner.FruitGrid[(int)FruitPosition.x, (int)FruitPosition.y] = null;
            FruitPosition = vector2tmp;
            FruitSpawner.Spawner.FruitGrid[(int)FruitPosition.x, (int)FruitPosition.y] = gameObject;
            Move.pos = FruitSpawner.rootPos.y + FruitPosition.y * FruitSpawner.RowSpace;
            Move.enabled = true;
        }
    }

    public void SetNewPosition()
    {
        FruitPosition = FruitPositionFinder(FruitPosition);
    }

    Vector2 FruitPositionFinder(Vector2 currentpos)
    {

        int x = (int)currentpos.x;
        int y = (int)currentpos.y;
        newpos = y;
        for (int i = y - 1; i >= 0; i--)
        {
            if (FruitSpawner.Spawner.FruitGrid[x, i] == null)
                newpos = i;
        }
        return new Vector2(x, newpos);
    }
    public Vector2 FruitPositionFinder(int x)
    {

        int y = 7;
        newpos = y;
        for (int i = y - 1; i >= 0; i--)
        {
            if (FruitSpawner.Spawner.FruitGrid[x, i] == null)
                newpos = i;
        }
        return new Vector2(x, newpos);
    }

}
