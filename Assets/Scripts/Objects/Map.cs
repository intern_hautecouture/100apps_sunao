﻿using UnityEngine;
using System.Collections;
using UnityEngine.iOS;

public class Map : MonoBehaviour
{

    public Player player;
    public SpriteRenderer[] Number;
    public Sprite[] NumberSprite;
    public Sprite[] backgroundSprite;
    public SpriteRenderer Stars;
    public Sprite[] StartsSprite;

    private int d, c, t;

    void OnMouseDown()
    {
        if (!player.Locked)
        {
            Sound.sound.click();
            GroupMoveControl.Clicked = true;
            Animation anim = GetComponent<Animation>();
            anim.enabled = true;
#if UNITY_IOS
            if (Device.generation.ToString().Contains("iPhoneX") || Device.generation.ToString().Contains("iPhoneUnknown"))
            {
                anim.Play("ButtonPressIphoneX");
            }
            else
            {
                anim.Play("ButtonPress");
            }
#else
            anim.Play("ButtonPress");
#endif
        }
    }

    void OnMouseUp()
    {
        if (!player.Locked)
        {
            Animation anim = GetComponent<Animation>();
            anim.enabled = true;
#if UNITY_IOS
            if (Device.generation.ToString().Contains("iPhoneX") || Device.generation.ToString().Contains("iPhoneUnknown"))
            {
                anim.Play("ButtonCancelIphoneX");
            }
            else
            {
                anim.Play("ButtonCancel");
            }
#else
            anim.Play("ButtonCancel");
#endif
        }
    }
    public void Play()
    {
        if (GroupMoveControl.Clicked)
        {
            ButtonActionController.s_Item(player);
        }
    }

    /// <summary>
    /// set infomation player by level
    /// </summary>
    /// <param name="index">level ID</param>
    public void SetPlayer(int index)
    {
        player = new Player();
        player.Level = PlayerPrefs.GetInt(MapManager.LEVEL_ + index);
        player.Locked = PlayerPrefs.GetString(MapManager.LOCKED_ + index) == "0" ? true : false;
        player.Stars = PlayerPrefs.GetInt(MapManager.STARS_ + index);
        player.Target = PlayerPrefs.GetInt(MapManager.TARGET_ + index);
        player.HightScore = PlayerPrefs.GetInt(MapManager.HIGH_SCORE_ + index);
        player.GameTime = PlayerPrefs.GetFloat(MapManager.GAME_TIME_ + index);
    }

    /// <summary>
    /// set sprite level is locked or no, star, number
    /// </summary>
    public void SetMap()
    {
        SetSpriteRender();
        setStars();
        SetlevelNumber();
    }
    private void SetSpriteRender()
    {
        if (player.Locked)
        {

            GetComponent<SpriteRenderer>().sprite = backgroundSprite[0];
            Stars.gameObject.SetActive(false);
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = backgroundSprite[1];
            Stars.gameObject.SetActive(true);
        }
    }

    private void setStars()
    {
        Stars.sprite = StartsSprite[player.Stars];
    }

    private void SetlevelNumber()
    {

        if (player.Locked)
        {
            Number[0].sprite = null;
            Number[1].sprite = null;
            Number[2].sprite = null;
        }
        else
        {
            SetNumber(Number);
        }
    }

    private void SetNumber(SpriteRenderer[] number)
    {
        d = player.Level % 100;
        d = d % 10;
        c = player.Level % 100;
        c = c / 10;
        t = player.Level / 100;

        if (t > 0)
        {
            number[0].sprite = NumberSprite[t];
            number[1].sprite = NumberSprite[c];
            number[2].sprite = NumberSprite[d];
            number[0].transform.localPosition = new Vector3(-0.74f, 0);
            number[1].transform.localPosition = new Vector3(0, 0);
            number[2].transform.localPosition = new Vector3(0.74f, 0);
        }
        else if (c > 0)
        {
            number[0].sprite = NumberSprite[c];
            number[1].sprite = NumberSprite[d];
            number[2].sprite = null;
            number[0].transform.localPosition = new Vector3(-0.39f, 0);
            number[1].transform.localPosition = new Vector3(0.39f, 0);
        }
        else
        {
            number[0].sprite = NumberSprite[d];
            number[1].sprite = null;
            number[2].sprite = null;
            number[0].transform.localPosition = new Vector3(0, 0);
        }
    }
}
